Descriptive Statistics - Exercises
================

``` r
library(dplyr)
library(data.table)
library(tidyverse)
library(reshape2)
library(ggplot2)
library(Rlab)
```

Load Data

``` r
PATH <- paste(getwd(), "data", sep="/")
baseball <- read.csv(paste(PATH, "baseball.csv", sep="/"))
compensation <- read.csv(paste(PATH, "Employee.Compensation.csv", sep="/"))
```

Exercise 1: Calculating Central Tendency
----------------------------------------

### Use the "baseball.csv" data set. Choose five variables in the data and for each:

#### Calculate the mean, median and mode.

``` r
getMode <- function(vals) {
  # https://www.tutorialspoint.com/r/r_mean_median_mode.htm
  uniqv <- unique(vals)
  return( uniqv[which.max(tabulate(match(vals, uniqv)))] )
}

print_mean_med_mode <- function(data, col) {
  print(col)
  cat(sprintf("The mean of the '%s' column is %.2f. \n", col, mean(data[[col]])))
  cat(sprintf("The median of the '%s' column is %.2f. \n", col, median(data[[col]])))
  cat(sprintf("The mode of the '%s' column is %.2f. \n", col, getMode(data[[col]])))
}
```

``` r
# Opt 1
names(baseball)
```

    ##  [1] "X"     "id"    "year"  "stint" "team"  "lg"    "g"     "ab"   
    ##  [9] "r"     "h"     "X2b"   "X3b"   "hr"    "rbi"   "sb"    "cs"   
    ## [17] "bb"    "so"    "ibb"   "hbp"   "sh"    "sf"    "gidp"

``` r
targets <- c("bb", "hr", "ab", "r", "h")
ex1 <- subset(baseball[targets], !is.na(targets))

for (each in targets) {
  print_mean_med_mode(ex1, each)
  print("-------------------------")
}
```

    ## [1] "bb"
    ## The mean of the 'bb' column is 22.49. 
    ## The median of the 'bb' column is 11.00. 
    ## The mode of the 'bb' column is 0.00. 
    ## [1] "-------------------------"
    ## [1] "hr"
    ## The mean of the 'hr' column is 5.23. 
    ## The median of the 'hr' column is 1.00. 
    ## The mode of the 'hr' column is 0.00. 
    ## [1] "-------------------------"
    ## [1] "ab"
    ## The mean of the 'ab' column is 225.40. 
    ## The median of the 'ab' column is 131.00. 
    ## The mode of the 'ab' column is 0.00. 
    ## [1] "-------------------------"
    ## [1] "r"
    ## The mean of the 'r' column is 31.78. 
    ## The median of the 'r' column is 15.00. 
    ## The mode of the 'r' column is 0.00. 
    ## [1] "-------------------------"
    ## [1] "h"
    ## The mean of the 'h' column is 61.76. 
    ## The median of the 'h' column is 32.00. 
    ## The mode of the 'h' column is 0.00. 
    ## [1] "-------------------------"

``` r
# Opt 2
sapply(baseball[targets], mean)
```

    ##         bb         hr         ab          r          h 
    ##  22.494723   5.234204 225.404903  31.780497  61.756901

``` r
sapply(baseball[targets], median)
```

    ##  bb  hr  ab   r   h 
    ##  11   1 131  15  32

``` r
# or
sapply(baseball[targets], summary)
```

    ##                bb        hr       ab        r        h
    ## Min.      0.00000  0.000000   0.0000   0.0000   0.0000
    ## 1st Qu.   1.00000  0.000000  25.0000   2.0000   4.0000
    ## Median   11.00000  1.000000 131.0000  15.0000  32.0000
    ## Mean     22.49472  5.234204 225.4049  31.7805  61.7569
    ## 3rd Qu.  38.00000  7.000000 435.0000  58.0000 119.0000
    ## Max.    232.00000 73.000000 705.0000 177.0000 257.0000

``` r
sapply(baseball[targets], getMode) # can't find a way to do this natively in R
```

    ## bb hr ab  r  h 
    ##  0  0  0  0  0

#### Explain for each variable, which of the three central tendencies is the best to represent the variable and why.

Each of these variables display a skewness from the normal distribution (central tendencies do not match). Due to this, the median is better suited than the mean. The counts of each value can be seen below.

``` r
# https://stackoverflow.com/questions/44295144/frequency-of-values-per-column-in-table
ex1 %>%
  melt() %>%
  dcast(value ~ variable, fun.aggregate = length) %>%
  head
```

    ##   value   bb   hr   ab    r    h
    ## 1     0 4753 9656 2178 4204 3399
    ## 2     1 1444 2275  398 1189  837
    ## 3     2 1069 1323  299  857  497
    ## 4     3  779  983  247  727  460
    ## 5     4  621  760  206  628  396
    ## 6     5  508  669  184  556  329

#### Are any of the measures for the central tendency misleading? Why?

Using the mean as a measure of central tendency can be misleading when there is skew in the data, and/or there are significant outliers. The mean might be far away from "most of the data", the center of the distribution. The median is a better indicator of the central tendency in such cases because it stays closer to the distribution center.

*Bonus: produce a histogram of the data, with vertical lines showing the mean, median, and mode.*

``` r
hist_mean_med_mode <- function(df, col, bins=300, xlim=c(-1,50)) {
  data <- subset(df, !is.na(df[[col]]))

  plot <- ggplot(data = data, aes(x=data[[col]], y=..count..)) +
            geom_histogram(bins=bins, fill="lightblue", binwidth = 0.5)

  max_count <- max(ggplot_build(plot)$data[[1]]$count)
  return ( plot +
            geom_vline(aes(xintercept=mean(data[[col]]), color="mean")) +
            geom_text(aes(label = "Mean", x=mean(data[[col]]), y=max_count/2,hjust=-0.1, color="mean")) +
            geom_vline(aes(xintercept=median(data[[col]]), color="median")) +
            geom_text(aes(label = "Median", x=median(data[[col]]), y=max_count/2.5,hjust=-0.1, color="median")) +
            geom_vline(aes(xintercept=getMode(data[[col]]), color="mode")) +
            geom_text(aes(label = "Mode", x=getMode(data[[col]]), y=max_count/3,hjust=-0.1, color="mode")) +
            theme(text=element_text(size=16)) +
            scale_color_manual(name = "Statistics", values=c(mean="red", median="blue", mode="orange")) +
            coord_cartesian(xlim=xlim) +
            theme_minimal())
  
}
```

``` r
hist_mean_med_mode(ex1, "bb")+
  labs(title = "Baseball Stat - Base on Balls (Walk) - Histogram", 
       y = "Count", 
       x = "Base on Balls - per player")
```

![](images/unnamed-chunk-8-1.png)

``` r
hist_mean_med_mode(ex1, "hr")+
  labs(title = "Baseball Stat - Home Runs - Histogram", 
       y = "Count", 
       x = "Home Runs - per player")
```

![](images/unnamed-chunk-9-1.png)

``` r
hist_mean_med_mode(ex1, "ab", xlim=c(0, 300),bins=500)+
  labs(title = "Baseball Stat - At Bat - Histogram", 
       y = "Count", 
       x = "At Bat - per player")
```

![](images/unnamed-chunk-10-1.png)

``` r
summary(ex1$ab)
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##     0.0    25.0   131.0   225.4   435.0   705.0

------------------------------------------------------------------------

Exercise 2: Detect Outliers with IQR Rule
-----------------------------------------

### Use the "Employee.compensation.csv" data set.

*Helpful R-functions: quantile(), IQR(), boxplot()*

``` r
comp <- compensation$Total.Compensation
mean_comp <- mean(comp)
sd_comp <- sd(comp)
```

#### Identify outliers for the variable "Total.Compensation".

``` r
ggplot(compensation, aes(x = Year, group = Year,y=Total.Compensation)) +
  geom_boxplot(outlier.color = "red") + 
  labs(title = "Compensation - per Year (Outliers in red)", x = "Year", y = "Total Compensation") +
  theme(text = element_text(size = 16)) +
  theme_minimal()
```

![](images/unnamed-chunk-14-1.png)

#### Explain what rule applies in the outlier and how you could identify them in the R-Output.

Criterion to identify outliers depends on the topic or problem at hand. However, there are several predetermined criterion and tests which could be used in general cases. The function `boxplot.stats`, as read [here](https://www.r-bloggers.com/identify-describe-plot-and-remove-the-outliers-from-the-dataset/), uses [Tukey's fences](https://en.wikipedia.org/wiki/Outlier#Tukey's_fences) to determine which data points would be considered outliers.

The bounds for Tukey's fences are as follows:

*\[Q<sub>1</sub> - k(Q<sub>3</sub> - Q<sub>1</sub>), Q<sub>3</sub> + k(Q<sub>3</sub> - Q<sub>1</sub>)\]*

where *Q<sub>1</sub>*, *Q<sub>3</sub>* are the lower and upper quartiles, and *k* is a non-negative constant. *k* = 1.5 is an outlier.

``` r
tukeys_lower <- quantile(comp, 1/4)
tukeys_upper <- quantile(comp, 3/4)
tukeys_iqr <- tukeys_upper - tukeys_lower
k <- 1.5
tukeys_outliers <- subset(comp, comp < (tukeys_lower - k*tukeys_iqr) | comp > tukeys_upper + k*tukeys_iqr)

cat(sprintf("Using Tukey's Fences, %d outliers are identified.", length(tukeys_outliers)))
```

    ## Using Tukey's Fences, 644 outliers are identified.

``` r
cat(sprintf("Using boxplot.stats, %d outliers are identified.", length(boxplot.stats(comp)$out)))
```

    ## Using boxplot.stats, 644 outliers are identified.

*Bonus: Detect outliers with the rule \[mean - t \* SD, mean + t \* SD\], with t = 3.*

``` r
sig <- 3
outlier_comp <- subset(comp, comp <= (mean_comp - sig*sd_comp) | comp >= (sig*sd_comp + mean_comp))

cat(sprintf("From the %d records, there are %d records with total compensation exceeding %d sigma from the mean compensation, $%.2f.", 
            length(comp), length(outlier_comp), sig, mean_comp))
```

    ## From the 213202 records, there are 650 records with total compensation exceeding 3 sigma from the mean compensation, $98472.69.

------------------------------------------------------------------------

Exercise 3: Standard deviations
-------------------------------

#### What is the population standard deviation for the numbers: 75, 83, 96, 100, 121 and 125?

``` r
nums <- c(75,83,96,100,121,125)
cat(sprintf("The standard deviation of the numbers %s is %.2f", toString(nums),sd(nums)))
```

    ## The standard deviation of the numbers 75, 83, 96, 100, 121, 125 is 19.98

``` r
pop_sd <- function(vals) {
  mn <- mean(vals)
  sqrd <- lapply(vals, function(x) (x - mn)**2)
  return(sqrt(Reduce("+", sqrd)/length(vals)))
}
```

``` r
cat(sprintf("The 'population' standard deviation of the numbers %s is %.2f.", toString(nums), pop_sd(nums)))
```

    ## The 'population' standard deviation of the numbers 75, 83, 96, 100, 121, 125 is 18.24.

#### Ten friends scored the following marks in their end-of-year math exam: 23%, 37%, 45%, 49%, 56%, 63%, 63%, 70%, 72% and 82%. What was the standard deviation of their marks?

``` r
scores <- c(23, 37, 45, 56, 63, 63, 70, 72, 82)

cat(sprintf("The standard deviation of the scores %s is %.2f.", toString(paste(scores, "%", sep="")), sd(scores)))
```

    ## The standard deviation of the scores 23%, 37%, 45%, 56%, 63%, 63%, 70%, 72%, 82% is 18.68.

#### A booklet has 12 pages with the following numbers of words: 271, 354, 296, 301, 333, 326, 285, 298, 327, 316, 287 and 314. What is the standard deviation number of words per page?

``` r
words_page <- c(271, 354, 296, 301, 333, 326, 285, 298, 327, 316, 287, 314)

cat(sprintf("The standard deviation of 'words per page's %s is %.2f", toString(words_page), sd(words)))
```

    ## Warning in var(if (is.vector(x) || is.factor(x)) x else as.double(x), na.rm
    ## = na.rm): NAs introduced by coercion

    ## The standard deviation of 'words per page's 271, 354, 296, 301, 333, 326, 285, 298, 327, 316, 287, 314 is NA

#### How does the standard deviation change, when each of the five numbers is increased by 2. Argue non-numerically.

#### Please write down your calculations, if computations are done in R, write down which approach was used.

    If every number above were to be increased by 2, there would be no increase in standard deviation. The standard deviation gives a measure of _deviation_ between values. If all values change in unison, their relative differences remain constant. 

``` r
words_aug <- unlist(lapply(words_page, function(x) x + 2))

cat(sprintf("The standard deviation of augmented 'words per page's %s is %.2f", toString(words_aug), sd(words_aug)))
```

    ## The standard deviation of augmented 'words per page's 273, 356, 298, 303, 335, 328, 287, 300, 329, 318, 289, 316 is 23.64

``` r
cat(sprintf("The 'population' standard deviation of augmented 'words per page's %s is %.2f", toString(words_aug), pop_sd(words_aug)))
```

    ## The 'population' standard deviation of augmented 'words per page's 273, 356, 298, 303, 335, 328, 287, 300, 329, 318, 289, 316 is 22.63

*Bonus: write an R-function for the computation of the variance either with a for-loop or with the function apply() (more advanced). Show for one of the examples above that your function is correct.*

``` r
variance <- function(vals) {
  mn <- sum(vals)/length(vals)
  return (Reduce("sum", 
                 lapply(vals, 
                        function(x) (x - mn)**2))/length(vals))
}
```

``` r
variance(nums)
```

    ## [1] 332.6667

``` r
pop_sd(nums) == sqrt(variance(nums))
```

    ## [1] TRUE

------------------------------------------------------------------------

Exercise 4: Population and Sample Standard Deviations
-----------------------------------------------------

### Answer the following questions and shortly explain your answers:

#### A teacher sets an exam for their pupils. The teacher wants to summarize the results the pupils attained as a mean and standard deviation. Which standard deviation should be used?

With a set of discrete values, like a group of scores from students, the population standard deviation is used because the entire population of students can be gathered and calculated.

#### A researcher has recruited males aged 45 to 65 years old for an exercise training study to investigate risk markers for heart disease (e.g., cholesterol). Which standard deviation would most likely be used?

With an unknown population size, a sample standard deviation is used. Random samples are taken from the *general* population of males between 45 and 65 years old, and their data is used for the calculations.

#### One of the questions on a national consensus survey asks for respondents' age. Which standard deviation would be used to describe the variation in all ages received from the consensus?

Like the first question, the population standard deviation is used, assuming that the survey covers the population as a whole. If there is a significant number of the population that didn't participate, the sample standard deviation might be appropriate.

------------------------------------------------------------------------

Exercise 5: Distributions
-------------------------

### Generate the following figure of a normal distribution with different parameters values using rnorm().

![distributions](images/normal_distribution.png)

<sub>Source: <https://en.wikipedia.org/wiki/Normal_distribution></sub>

``` r
mean_sd_name <- function(lst) {
  return(sprintf("mean=%d, var=%.1f", get("mean", lst), get("sd", lst)**2))
}
```

``` r
# https://ggplot2.tidyverse.org/reference/stat_function.html
n <- 10000
one = list(mean=0, sd = sqrt(0.2))
two = list(mean=0, sd = sqrt(1))
three = list(mean=0, sd = sqrt(5))
four = list(mean=-2, sd = sqrt(0.5))

label1 = mean_sd_name(one)
label2 = mean_sd_name(two)
label3 = mean_sd_name(three)
label4 = mean_sd_name(four)

color_vals = c("blue", "red", "orange", "green")
# https://stackoverflow.com/questions/4253201/how-to-put-in-labels-in-list-for-r
thing = setNames(color_vals, c(label1, label2, label3, label4))

ggplot(data.frame(x=c(-5,5), y=c(0,1)), aes(x=x, y=y)) +
  stat_function(fun = dnorm, n = n, args = one,   aes(color=label1)) +
  stat_function(fun = dnorm, n = n, args = two,   aes(color=label2)) +
  stat_function(fun = dnorm, n = n, args = three, aes(color=label3)) +
  stat_function(fun = dnorm, n = n, args = four,  aes(color=label4)) +
  scale_color_manual(name="", values=color_vals) +
  scale_y_continuous(limits=c(0,1))+
  labs(title = "Normal Distributions",
       y = "dnorm(mean,sd)",
       x = "x") +
  theme_minimal()
```

![](images/unnamed-chunk-26-1.png)

### Explain how the shape of the distribution is altered when the two parameters are altered.

The center (peak) of the distribution follows the mean. When the mean increases, the distribution moves right. When the mean decreases, the distribution moves left. The 'spread' of the distribution changes with the variance (standard deviation <sup>2</sup>). As the variance increases, the distribution spreads out, flattening towards the x-axis. As the variance decreases, the distribution *pulls* inwards, making a steeper plot centered on the mean.

*Bonus: Do the same for a Poisson and/or the exponential distribution (Parameter: ??).*

``` r
lambda_name <- function(val) {
  return(sprintf("lambda=%d",val))
}
```

``` r
n <- 10000
one = 1
two = 5
three = 10
four = 15

label1 = lambda_name(one)
label2 = lambda_name(two)
label3 = lambda_name(three)
label4 = lambda_name(four)

color_vals = c("blue", "red", "orange", "green")
# https://stackoverflow.com/questions/4253201/how-to-put-in-labels-in-list-for-r
thing = setNames(color_vals, c(label1, label2, label3, label4))

ggplot(data.frame(x=c(0:10)), aes(x=x) )+ 
  geom_line(aes(y=dpois(x, one), color=label1))+
  geom_line(aes(y=dpois(x, two), color=label2))+
  geom_line(aes(y=dpois(x, three), color=label3))+
  geom_line(aes(y=dpois(x, four), color=label4))+
  scale_color_manual(name="", values=color_vals) +
  labs(title="Poisson Distributions",
       y = "dpois(lambda)",
       x = "x") +
  theme_minimal()
```

![](images/unnamed-chunk-28-1.png)

``` r
rate_name <- function(val) {
  return(sprintf("rate=%.2f",val))
}
```

``` r
n <- 10000
one = 1
two = 5
three = 1.5
four = 0.5

label1 = rate_name(one)
label2 = rate_name(two)
label3 = rate_name(three)
label4 = rate_name(four)

color_vals = c("blue", "red", "orange", "green")
# https://stackoverflow.com/questions/4253201/how-to-put-in-labels-in-list-for-r
thing = setNames(color_vals, c(label1, label2, label3, label4))

ggplot(data.frame(x=c(-10:10)), aes(x=x) )+ 
  geom_line(aes(y=dexp(x, one), color=label1))+
  geom_line(aes(y=dexp(x, two), color=label2))+
  geom_line(aes(y=dexp(x, three), color=label3))+
  geom_line(aes(y=dexp(x, four), color=label4))+
  scale_color_manual(name="", values=color_vals) +
  coord_cartesian(xlim=c(-5,10))+
  labs(title="Exponential Distribution",
       y = "dexp(rate)",
       x = "x") +
  theme_minimal()
```

![](images/unnamed-chunk-30-1.png)

------------------------------------------------------------------------

Exercise 6: Probability Density Function and Cumulative Distribution Function
-----------------------------------------------------------------------------

### Assume you have a population of people whose heights are described by Height ~ N (170, 7):

*μ* = 170, *σ* = 7

``` r
mu <- 170
sig <- 7
```

#### What percent of the population is between 170 and 175cm tall?

``` r
z1 <- (175 - mu)/sig
z0 <- (170 - mu)/sig
cat(sprintf("The percentage of the population between 170 - 175cm is %.1f%%.", 100*(pnorm(z1) - pnorm(z0))))
```

    ## The percentage of the population between 170 - 175cm is 26.2%.

#### What height is taller than 70% of the population?

``` r
cat(sprintf("The height that is taller than 70%% of the population is %.2fcm", qnorm(0.7, mean=mu, sd=sig)))
```

    ## The height that is taller than 70% of the population is 173.67cm

#### If someone is 168cm tall, what percent of the population is shorter than they are?

``` r
cat(sprintf("Someone with a height of 168cm is taller than %.2f%% of the population", 100*(pnorm(168, mean=mu, sd=sig))))
```

    ## Someone with a height of 168cm is taller than 38.75% of the population

``` r
heights_range <- function(mean, sd, perc) {
  half <- perc/2
  cat(sprintf("Heights between %.2fcm and %.2fcm include %.1f%% of the population\n", 
              qnorm(0.5-half, mean=mean, sd=sd), qnorm(0.5+half, mean=mean, sd=sd), perc*100))
}
```

#### Find an interval that includes 50% of the population.

``` r
heights_range(mu,sig,0.5)
```

    ## Heights between 165.28cm and 174.72cm include 50.0% of the population

``` r
cat(sprintf("Heights between %.2fcm and %.2fcm include 50%% of the population\n", qnorm(0.49, mean=mu, sd=sig), qnorm(0.99, mean=mu, sd=sig)))
```

    ## Heights between 169.82cm and 186.28cm include 50% of the population

``` r
cat(sprintf("Heights between %.2fcm and %.2fcm include 50%% of the population\n", qnorm(0.01, mean=mu, sd=sig), qnorm(0.49, mean=mu, sd=sig)))
```

    ## Heights between 153.72cm and 169.82cm include 50% of the population

#### Find an interval that includes 20% of the population.

``` r
heights_range(mu,sig,0.2)
```

    ## Heights between 168.23cm and 171.77cm include 20.0% of the population

``` r
heights_range(mu,sig,0.6471)
```

    ## Heights between 163.50cm and 176.50cm include 64.7% of the population

------------------------------------------------------------------------

Exercise 7: Bernoulli / Binomial Distribution
---------------------------------------------

### Implement your own function called "myBinomial" that has two arguments:

*n* is the number of draws to return *p* is the parameter for the Bernoulli distribution: the probability of "success" Return a vector of the n random draws from a Bernoulli distribution with the given 'p' and calculate the expected value and the variance. Explain the connection between Bernoulli and Binomial.

*Hint: runif(1) returns a value between 0 and 1, then you can see whether it meets the criteria value of 'p'.*

> myBinomial &lt;- function(n, p){ \#some stuff return(vector\_of\_draws) }

*Bonus: Find a different solution to your function or find the most computationally efficient solution.*

``` r
myBernoulli <- function(n, p) {
  # return(rbern(n, p))
  
  # return(unlist(lapply(
  #   rep.int(1, n), 
  #   function(x) ifelse(runif(1) > p, yes = 1, no = 0))))
  
  return(unlist(lapply(
    runif(n) > p,
    function(x) ifelse(runif(1) < p, yes = 1, no = 0)
  )))
}
```

``` r
n_trials <- 1000
p <- 0.25
bern_title <- sprintf("Bernoulli Distribution - %d Trials, %.1f%% probability of success", n_trials, 100*p)
bern_freq <- as.data.frame(table(myBernoulli(n_trials, p)))

ggplot(bern_freq, aes(x=Var1, y=Freq)) + 
  geom_bar(stat="identity",aes(fill=Freq)) + 
  geom_text(aes(x=1, y=n_trials/2, label="Failures")) +
  geom_text(aes(x=2, y=n_trials/2, label="Successes")) +
  labs(title=bern_title,
       x = "Failure or Success")
```

![](images/unnamed-chunk-40-1.png)

``` r
singleBin <- function(n,p,k) {
  prob <- (p**k)*((1-p)**(n-k))
  return(choose(n,k)*prob)
}

myBinomial <- function(n, p) {
  # return(dbinom(seq(0, n, by=1), n, p))
  
  currentBin <- function(n,prob) function(k) singleBin(n,p,k)
  return(unlist(lapply(
    seq(1,n),
    currentBin(n,p))))
}
```

``` r
n_trials <- 100
p <- 0.25
bin_title <- sprintf("Binomial Distribution - %d Trials, %.1f%% probability of success", n_trials, 100*p)

bin_df <- data.frame(y=myBinomial(n_trials, p), x=seq(1, n_trials))

ggplot(bin_df, aes(x,x,y=y)) +
  geom_point() +
  labs(title = bin_title)
```

![](images/unnamed-chunk-42-1.png)

So, for a given Binomial Distribution derived over *n* trials, each with a *p* probability of success, the distribution centers around the expected number of *successes* in a Bernoulli Distribution.

------------------------------------------------------------------------
