Rolling Dice and Expected Values
================

``` r
library(ggplot2)
library(Rdice)
```

#### `Rdice` is a nice library with a convenient `dice.roll` function

``` r
faces = 6
trials = 100
trial <- dice.roll(faces = faces, dice = 1, rolls = trials)
head(trial$frequencies, 5)
```

    ##    values  N freq
    ## 1:      1 13 0.13
    ## 2:      2 15 0.15
    ## 3:      3 17 0.17
    ## 4:      4 15 0.15
    ## 5:      5 16 0.16

``` r
head(trial$sums_freq, 5)
```

    ##    sum  N freq cum_sum
    ## 1:   1 13 0.13    0.13
    ## 2:   2 15 0.15    0.28
    ## 3:   3 17 0.17    0.45
    ## 4:   4 15 0.15    0.60
    ## 5:   5 16 0.16    0.76

#### Visualize frequency of rolls from `dice.roll`

``` r
ggplot(trial$frequencies, aes(x=values, y=freq)) +
  geom_bar(stat="identity") +
  labs(title=sprintf("Frequency of Rolls over %d Trials with a single %d face die", trials, faces),
       x = "Possible Values",
       y = sprintf("Frequency over %d Trials", trials)) +
  theme_minimal()
```

![](images/dice_6_100_freq-1.png)

#### The expected value of any given roll from some *n* faced dice can be estimated over a series of trials

``` r
roll_dice <- function(f) function(x) {
  trial <- dice.roll(faces=f, dice=1, rolls=x)
  return(trial$exp_value_sum)
}

dice_trials <- function(faces, trials) {
  roller <- roll_dice(faces)
  all_trials <- seq(1, trials)
  results <- lapply(all_trials, roller)
  return(unlist(results))
}
```

``` r
plot_dice_trials <- function(df, faces, trials) {
  mean_trials <- mean(df$rolls)
  return(ggplot(df, aes(x=x, y=rolls)) +
          geom_line() + 
          geom_hline(yintercept = mean_trials, color="red") +
          geom_text(aes(x=max(df$x), y=mean_trials, vjust=-2, hjust=1), 
                    label=sprintf("Final Expected Value: %.2f", mean_trials)) +
          labs(title=sprintf("Dice Rolls - %d Trials, %d Faces on 1 die", trials, faces),
               y = "Expected Value of a roll",
               x = "Trial")+
          scale_y_continuous(limits=c(1, faces)) +
          theme_minimal())
}
```

#### Rolling a 6-faced die 1000 times

``` r
n_faces = 6
n_trials = 1000
dice6_df <- data.frame(rolls=dice_trials(n_faces, n_trials), x=seq(1,n_trials))
```

``` r
plot_dice_trials(dice6_df, n_faces, n_trials)
```

![](images/dice_6_1000-1.png)

#### Rolling a 10-faced die 1000 times

``` r
n_faces = 10
n_trials = 1000
dice10_df <- data.frame(rolls=dice_trials(n_faces, n_trials), x=seq(1,n_trials))
```

``` r
plot_dice_trials(dice10_df, n_faces, n_trials)
```

![](images/dice_10_1000-1.png)
