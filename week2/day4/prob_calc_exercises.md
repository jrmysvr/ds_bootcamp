Probability Calculus - Exercises
================

``` r
library(ggplot2)
```

### Exercise 1: Sampling distribution

#### Assume that the weights of 10-year-old children are normally distributed with a mean of 90 and a standard deviation of 36. What is the sampling distribution of the mean for a sample size of 9?

The *mean* of the *sampling distribution of the mean* is the same as the *mean* of the population: *μ*<sub>M</sub> = *μ*

The *variance* of the *sampling distribution of the mean* is: *σ*<sub>M</sub> = $\\frac{\\sigma{}}{\\sqrt{N}}$

, where *N* is the sample size. So, the variance of the *sampling distribution of the mean* decreases as the sample size increases.

With a sample size of 9, *μ*<sub>M</sub> = 90 and *σ*<sub>M</sub> = $\\frac{36}{\\sqrt{9}}$ = 12.

Exercise 2: Interval and Point estimates
----------------------------------------

### State for each statement whether it is TRUE or FALSE, explain why:

#### When the margin of error is small, the confidence level is high.

#### When the margin of error is small, the confidence level is low.

The *margin of error* is based on the *confidence level*. When a claim is made and sampling occurs to assess that claim, a *test statistic* ± *margin of error* establishes a *confidence interval* for potential values. To define the bounds of the *confidence interval*, a *confidence level* is incorporated using $\\hat{\\theta{}}$ ± *z*<sub>$\\frac{1-\\alpha{}}{2}$</sub>$\\sqrt{\_Var\_(\\hat{\\theta{}})}$, where *α* is commonly 95%.

Generally speaking, though, if a *margin of error* is large, the confidence interval is wider, so there is a wider range of values and a higher chance of capturing the claim. In conrast, a small *margin of error* results in a narrow confidence interval and less confidence in the claim.

#### A confidence interval is a type of point estimate.

A *point estimate* is reported with a *confidence interval*. For a claim, an estimate is given with a range of values for the claim to be found. Alternatively, a range of values is reported with low and high bounds of the estimate.

#### A sample mean is an example of a point estimate.

True, a *sample mean* is a projection of the *mean* of the population that is being sampled.

Exercise 3: Confidence Intervals
--------------------------------

### A population is known to be normally distributed with a standard deviation of 2.8.

#### Compute the 95% confidence interval on the mean based on the following sample of nine: 8, 9, 10, 13, 14, 16, 17, 20, 21.

``` r
print_confidence_interval <- function(SD, confidence_level, samples) {
  margin <- abs(qnorm((1-confidence_level)/2)) * SD
  sample_mean <- mean(samples)
  
  confidence_interval <- c(sample_mean - margin, sample_mean + margin)
  cat(sprintf("The %.1f%% confidence interval of the sample mean based on values %s and a population SD of %.1f is [%.2f, %.2f].\n", 
              confidence_level*100, toString(samples), SD, confidence_interval[1], confidence_interval[2]))
}
```

``` r
sample <- c(8, 9, 10, 13, 14, 16, 17, 20, 21)
print_confidence_interval(2.8, 0.95, sample)
```

    ## The 95.0% confidence interval of the sample mean based on values 8, 9, 10, 13, 14, 16, 17, 20, 21 and a population SD of 2.8 is [8.73, 19.71].

#### Now compute the 99% confidence interval using the same data.

``` r
print_confidence_interval(2.8, 0.99, sample)
```

    ## The 99.0% confidence interval of the sample mean based on values 8, 9, 10, 13, 14, 16, 17, 20, 21 and a population SD of 2.8 is [7.01, 21.43].

------------------------------------------------------------------------

Exercise 4: CLT and LLN
-----------------------

### Generate sample means, $\\bar{x}$, for 50 samples of 100 numbers randomly spread between 0 and 1. The sampling distribution of $\\bar{x}$ is the distribution of the means from all possible samples.

``` r
test_population <- abs(rnorm(100))
test_population <- test_population/max(test_population)
test_sample <- sample(test_population, 50)

# Alternate random sampling
# summary(sample.int(100, 50)/100)
# test_population <- sample.int(100,100)/100
# test_sample <- sample(test_population, 50)

summary(test_sample)
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ## 0.01191 0.14052 0.28963 0.36750 0.54531 1.00000

``` r
summary(test_population)
```

    ##     Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
    ## 0.001516 0.144104 0.305001 0.364478 0.533982 1.000000

#### Make a histogram of the all 50 sample means.

``` r
stats = c("Mean"="red", "Median"="blue")
qplot(test_sample, bins=length(test_sample),
      main = "Test Sample Values",
      xlab = "Sample values",
      ylab = "Count") +
  geom_vline(aes(xintercept=mean(test_sample), color="Mean")) +
  geom_vline(aes(xintercept=median(test_sample), color="Median")) +
  scale_color_manual(name="Statistic", values=stats) +
  theme(text=element_text(size=12))+
  theme_minimal()
```

![](images/unnamed-chunk-6-1.png)

#### Give the Law of Large Numbers and the Central Limit Theorem in context of this exercise.

``` r
cat(sprintf("The population mean is %.2f and median is %.2f while the sample mean is %.2f and median is %.2f.\n", 
            mean(test_population), median(test_population), mean(test_sample), median(test_sample)))
```

    ## The population mean is 0.36 and median is 0.31 while the sample mean is 0.37 and median is 0.29.

``` r
cat(sprintf("The population sd is %.2f while the sample sd is %.2f.\n",
            sd(test_population), sd(test_sample)))
```

    ## The population sd is 0.25 while the sample sd is 0.27.

With a size of 50 elements, the sample group is within 10% of the mean and median of the population. This sample size is large enough to capture the statistics of the population, which is expected in the *Central Limit Theorem* where the sample group follows an approximate normal distribution representing the population.

The *Law of Large Numbers* says that, with enough samples of a population, the sample mean approaches the population mean. Again, based on the results, this can be confirmed at least with this experiment.

#### Compute the mean and median. Use the information and the histogram in (a). Does the distribution appear to be roughly normal, as the CLT states.

``` r
pop_vs_sample <- function(S, P, bins=length(P), bw=1, title="Histogram - Test Population vs Test Sample", norm=F) {
  
  check_sample <- function(sample, population) function(x) ifelse(x %in% sample, yes="sample", no="pop")
  checker <- check_sample(S, P)
  pop_df <- data.frame(pop=P, x=seq(1, length(P)))
  pop_df$group <- unlist(lapply(P, checker))
  
  stats = c("red", "blue", "orange", "green")
  pop_mean = mean(P)
  samp_mean = mean(S)
  pop_median = median(P)
  samp_median = median(S)
  
  pop_mean_label = sprintf("Pop Mean: %.3f", pop_mean)
  samp_mean_label = sprintf("Sample Mean: %.2f", samp_mean)
  pop_median_label = sprintf("Pop Median: %.2f", pop_median)
  samp_median_label = sprintf("Sample Median: %.2f", samp_median)
  
  stat_names = setNames(stats, c(pop_mean_label, samp_mean_label, pop_median_label, samp_median_label))
  
  plot <- ggplot(pop_df) +
            geom_histogram(aes(x=pop, fill=group), bins=bins, binwidth = bw) +
            geom_vline(aes(xintercept=pop_mean, color=pop_mean_label)) +
            geom_vline(aes(xintercept=samp_mean, color=samp_mean_label)) +
            geom_vline(aes(xintercept=pop_median, color=pop_median_label)) +
            geom_vline(aes(xintercept=samp_median, color=samp_median_label)) +
            scale_color_manual(name="Statistics", values=stat_names) +
            labs(title=title,
                 x = "Values",
                 y = "Count")
  
  plot <- plot + 
    # https://stackoverflow.com/questions/47916307/ggplot2-specify-geom-text-position-by-conventional-top-bottom-left-rig?rq=1
    geom_text(aes(label=sprintf("Pop SD: %.2f", sd(P)), 
                  x=Inf,
                  y=Inf,
                  hjust=1,
                  vjust=1)) +
    geom_text(aes(label=sprintf("Sample SD: %.2f", sd(S)), 
                  x=Inf,
                  y=Inf,
                  hjust=1,
                  vjust=2.5)) + 
    theme(text=element_text(size=12)) +
    theme_minimal()
              
  if (norm) {
    plot <- plot + 
      # https://stackoverflow.com/questions/6967664/ggplot2-histogram-with-normal-curve
        stat_function(
          fun = function(x, mean, sdv, bw) {
            dnorm(x = x, mean=mean, sd=sdv) * bw
          },
          args = c(mean=mean(P), sdv=sd(P), bw=length(P) * bw)
        )
  }
  
  return(plot)
}
```

``` r
pop_vs_sample(test_sample, test_population, bw=0.025, bins=length(test_population), norm=T)
```

![](images/unnamed-chunk-9-1.png)

------------------------------------------------------------------------

Exercise 5: Central Limit Theorem
---------------------------------

### Consider taking random samples of various sizes n from an exponential distribution. At what sample size n does the normal distribution make a good approximation to the actual distribution of the sample mean?

*Outline your approach in detail.*

\_Hint: first read the two examples of a possible approach: <https://onlinecourses.science.psu.edu/stat414/node/177/_>

``` r
n_exp <- 1000
exp_df <- data.frame(exp = rexp(n_exp, 1), x=seq(1, n_exp))
```

``` r
stats = c("red", "blue")

exp_mean = mean(exp_df$exp)
exp_median = median(exp_df$exp)

exp_mean_label = sprintf("Mean: %.2f", exp_mean)
exp_median_label = sprintf("Median: %.2f", exp_median)

stat_names = setNames(stats, c(exp_mean_label, exp_median_label))

qplot(x=exp, data=exp_df, bins=100,
      main = "Exponential Distribution - Histogram",
      ylab = "Count",
      xlab = "Values") +
  geom_vline(aes(xintercept=exp_mean, color=exp_mean_label)) +
  geom_vline(aes(xintercept=exp_median, color=exp_median_label)) +
  scale_color_manual(name="Statistics", values=stat_names) +
  theme_minimal()
```

![](images/unnamed-chunk-11-1.png)

``` r
pop_vs_sample(sample(exp_df$exp, 10), exp_df$exp, 
              bins=length(exp_df$exp), 
              bw=0.5,
              title="Exp Distribution - Histogram - 10 Samples vs Population") +
  coord_cartesian(xlim=c(-2,6))
```

![](images/unnamed-chunk-12-1.png)

``` r
pop_vs_sample(sample(exp_df$exp, 100), exp_df$exp, 
              bins=length(exp_df$exp), 
              bw=0.5,
              title="Exp Distribution - Histogram - 100 Samples vs Population") +
  coord_cartesian(xlim=c(-2,6))
```

![](images/unnamed-chunk-13-1.png)

``` r
pop_vs_sample(sample(exp_df$exp, 500), exp_df$exp, 
              bins=length(exp_df$exp), 
              bw=0.5,
              title="Exp Distribution - Histogram - 500 Samples vs Population") +
  stat_function (
        fun = function(x, rate, bw, n) {
          dexp(x, rate) * bw * n
        },
        args = c(rate=1, bw=0.5, n=1000)
  ) +
  stat_function (
        fun = function(x, rate, bw, n) {
          dexp(x, rate) * bw * n
        },
        args = c(rate=1, bw=0.5, n=500)
  ) 
```

![](images/unnamed-chunk-14-1.png)

``` r
chi_func <- stat_function (
                fun = function(x, bw, n) {
                  dchisq(x, 2) * bw * n
                },
                args = c(bw=1, n=1000)
              ) 
```

``` r
# Chi-squared distribution
chi <- rchisq(1000, 2)
pop_vs_sample(sample(chi, 50), chi,
              bw=1,
              bins=) + chi_func
```

![](images/unnamed-chunk-16-1.png)

``` r
pop_vs_sample(sample(chi, 100), chi,
              bw=1,
              bins=) + chi_func
```

![](images/unnamed-chunk-17-1.png)

``` r
pop_vs_sample(sample(chi, 500), chi,
              bw=1) + chi_func
```

![](images/unnamed-chunk-18-1.png)

``` r
pop_vs_sample(sample(chi, 500), chi,
              bw=1,
              norm=T) +
  coord_cartesian(xlim=c(-2.5,5))
```

![](images/unnamed-chunk-19-1.png)

Generally, from the plots, the variance of the sample approaches that of the population as the sample size increases. The mean follows the same trend.

Exercise 6: Bootstrapping
-------------------------

#### Take the sample: 8, 12, 58, 94, 103, 115, drawn from a population with unknown distribution

``` r
boot_sample <- c(8,12,58,94,103,115)
```

#### Re-sample with replacement 100 times with sample(x, size, replace=TRUE).

``` r
resample_mean <- function(x) function(ignore) mean(sample(x, length(x), replace=T))

resample_sd <- function(x) function(ignore) sd(sample(x, length(x), replace=T))

mean_sampler <- resample_mean(boot_sample)
sd_sampler <- resample_sd(boot_sample)
```

#### Compute a vector with the means for each re-sample.

``` r
n_resamples <- 100
boot_stats <- data.frame(
  means = unlist(lapply(rep(1, n_resamples), mean_sampler)),
  sds   = unlist(lapply(rep(1, n_resamples), sd_sampler))
)
```

#### Plot their sample means.

``` r
qplot(boot_stats$means, bins=20,
      main=sprintf("%d Resampled Means from %s", n_resamples, toString(boot_sample)),
      xlab="Values",
      ylab="Count") +
  theme_minimal()
```

![](images/unnamed-chunk-23-1.png)

``` r
qplot(boot_stats$sds, bins=20,
      main=sprintf("%d Resampled Standard Deviations from %s", n_resamples, toString(boot_sample)),
      xlab="Values",
      ylab="Count")+
  theme_minimal()
```

![](images/unnamed-chunk-24-1.png)

#### Calculate a 95% confidence interval for the mean of this population.

``` r
# Filters for establishing a confidence interval

filter_confidence <- function(err, val) {
  function(x) {
    ifelse(
      (x > (val - err)) &&
      (x < (val + err)),
      yes = "conf", no = "err"
    )
  }
}

filter_2sig <- function(mn, sdv) {
  function(x) {
    ifelse(
      (x > (mn - 2*sdv)) &&
      (x < (mn + 2*sdv)),
      yes = "conf", no = "err"
    )
  }
}
```

``` r
bootstrap_means_hist <- function(means,confidence_level=0.95, bw=1, bins=length(means)) {
  boot_mean <- mean(means)
  boot_sd <- sd(means)
  
  # confidence_level <- 0.95
  
  z <- abs(qnorm((1-confidence_level)/2))
  margin <- z*boot_sd

  margin_mean_95 <- filter_confidence(margin, boot_mean)
  # margin_mean_95 <- filter_2sig(boot_mean, boot_sd)
  
  low = boot_mean - margin
  high = boot_mean + margin
  # low = boot_mean - 2*boot_sd
  # high = boot_mean + 2*boot_sd
  
  low_label = sprintf("Low Bound: %.2f", low)
  high_label = sprintf("High Bound: %.2f", high)
  mean_label = sprintf("Mean: %.2f", boot_mean)
  
  stats = c("lightblue","lightblue", "red")
  
  stat_names = setNames(stats, c(low_label, high_label, mean_label))
  
  df <- data.frame(means = means)
  df$conf <- unlist(lapply(means, margin_mean_95))
  
  ggplot(df) +
    geom_histogram(aes(x=means, fill=conf), binwidth=bw, bins=bins) +
    geom_vline(aes(xintercept=low, color=low_label)) +
    geom_vline(aes(xintercept=high, color=high_label)) +
    geom_vline(aes(xintercept=boot_mean, color=mean_label)) +
    scale_color_manual(name="Statistics", values=stat_names) +
    labs(title = sprintf("Bootstrap Means - %.1f%% confidence interval", confidence_level*100),
         y = "Count",
         x = "Values") +
    theme_minimal() +
    stat_function(
      fun = function(x, mean, sd, bw, n) {
        dnorm(x = x, mean = mean, sd = sd) * bw * n
      },
      args=c(mean = boot_mean, sd = boot_sd, bw=bw, n=n_resamples)
    )
}
```

``` r
bootstrap_means_hist(boot_stats$means, bw=3, bins=100)
```

![](images/unnamed-chunk-27-1.png)

------------------------------------------------------------------------

Exercise 7: Bootstrapping
-------------------------

### The process above can be extended to other statistics besides the mean. Using your own data set (from your project, or one we used yesterday) try to implement a bootstrapping approach following along in this tutorial: <http://www.stat.wisc.edu/~larget/stat302/chap3.pdf>.

``` r
baseball_path <- paste(getwd(), "data", "baseball.csv", sep="/")
baseball = read.csv(baseball_path)
```

``` r
target = "ab"
sub_ab <- subset(baseball[[target]], !is.na(baseball[[target]]))
qplot(sub_ab, bins=length(sub_ab)/500,
      main =sprintf("Histogram - Baseball Data - Variable: %s", target),
      xlab = "Values",
      ylab = "Count")
```

![](images/unnamed-chunk-29-1.png)

``` r
boot_ab <- sample(sub_ab, length(sub_ab)/1000)
boot_ab
```

    ##  [1]   0  15  54  63 507 439   0 484 494   0 581 108 543 424 304   1 560
    ## [18]  98   0 348   4

#### Find the population mean and 95% CI of one variable in your data. How do you interpret the 95% CI values?

``` r
build_bootstrap_samples <- function(n, samples, stat_label) {
  if(stat_label == "mean"){
    output <- unlist(lapply(rep(1,n), resample_mean(samples)))
  } 
  else if (stat_label == "sd") {
    output <- unlist(lapply(rep(1,n), resample_sd(samples)))
  }
  return(output)
}
```

``` r
boot_samples <- 100
boot_ab_means <- build_bootstrap_samples(boot_samples, boot_ab, "mean")
bootstrap_means_hist(boot_ab_means, bins=50, bw=10)
```

![](images/unnamed-chunk-32-1.png)

``` r
bootstrap_means_hist(boot_ab_means, .7, bins=50, bw=10)
```

![](images/unnamed-chunk-33-1.png)

#### Approximate the population statistics by extending this approach also to other sample statistics.

``` r
bootstrap_stat_hist <- function(stats,stat_label="",confidence_level=0.95, bw=1, bins=length(stats)) {
  boot_mean <- mean(stats)
  boot_sd <- sd(stats)
  
  z <- abs(qnorm((1-confidence_level)/2))
  margin <- z*boot_sd

  margin_filter <- filter_confidence(margin, boot_mean)
  
  low = boot_mean - margin
  high = boot_mean + margin
  
  low_label = sprintf("Low Bound: %.2f", low)
  high_label = sprintf("High Bound: %.2f", high)
  mean_label = sprintf("Mean: %.2f", boot_mean)
  
  colors = c("lightblue","lightblue", "red")
  
  stat_names = setNames(colors, c(low_label, high_label, mean_label))
  
  df <- data.frame(stats = stats)
  df$conf <- unlist(lapply(stats, margin_filter))
  
  spacer <- ifelse(length(stat_label) > 0, yes=" ", no="")
  
  ggplot(df) +
    geom_histogram(aes(x=stats, fill=conf), binwidth=bw, bins=bins) +
    geom_vline(aes(xintercept=low, color=low_label)) +
    geom_vline(aes(xintercept=high, color=high_label)) +
    geom_vline(aes(xintercept=boot_mean, color=mean_label)) +
    scale_color_manual(name="Statistics", values=stat_names) +
    labs(title = sprintf("Bootstrap %s%s- %.1f%% confidence interval", stat_label, spacer, confidence_level*100),
         y = "Count",
         x = "Values") +
    stat_function(
      fun = function(x, mean, sd, bw, n) {
        dnorm(x = x, mean = mean, sd = sd) * bw * n
      },
      args=c(mean = boot_mean, sd = boot_sd, bw=bw, n=length(stats))
    ) +
    theme_minimal()
}
```

``` r
boot_ab_sd <- build_bootstrap_samples(1000, boot_ab, "sd")
bootstrap_stat_hist(boot_ab_sd, stat_label = "Standard Deviations", bw=10, bins=400, confidence_level = .9)
```

![](images/unnamed-chunk-35-1.png)

#### Calculate the 95% CI of the difference in means between two groups in your data (e.g. difference in average rating for role-playing games and puzzle games).

*Hint: You may want to do the tutorial following along with their data set before moving to your own data.*

``` r
# At bats vs home runs
target = "hr"
sub_hr <- subset(baseball[[target]], !is.na(baseball[[target]]))
qplot(sub_hr, bins=length(sub_hr)/500,
      main =sprintf("Histogram - Baseball Data - Variable: %s", target),
      xlab = "Values",
      ylab = "Count")+
  theme_minimal()
```

![](images/unnamed-chunk-36-1.png)

``` r
boot_ab
```

    ##  [1]   0  15  54  63 507 439   0 484 494   0 581 108 543 424 304   1 560
    ## [18]  98   0 348   4

``` r
boot_hr <- sample(sub_hr, length(sub_hr)/1000)
boot_hr
```

    ##  [1] 13  1  1  0  8  8  7 54  0  2 41  0  2  3  3 38  8  0 44  1  0

``` r
boot_hr_means <- build_bootstrap_samples(boot_samples, boot_hr, "mean")
```

``` r
bootstrap_stat_hist(boot_ab_means-boot_hr_means,
                    stat_label = "Difference in Means - At Bat vs. Home Runs",
                    bins = 100,
                    bw=10)
```

![](images/unnamed-chunk-39-1.png)

------------------------------------------------------------------------
