Exercise 4: Permutation Tests
================

``` r
library(gtools)
```

### Given the following observations, use a permutation test to decide if the treatment had an effect. In other words, is this grouping of observations into treatment and control groups likely to happen randomly?

##### Control: 112, 97, 101

##### Treatment: 123, 99, 119

``` r
# Group all observations
obsv = c(112, 97, 101, 123, 99, 119)
ctrl = c(112, 97, 101)
trt = c(123, 99, 119)
```

#### 1. Get all possible rearrangements of these observations into the two groups.

``` r
# combinations because order doesn't matter
obsv_comb = combinations(length(obsv), 3, obsv) 
rows = nrow(obsv_comb)

# grouping combinations
grp1 = obsv_comb[1:(rows/2),] #first half of combinations
grp2 = obsv_comb[rows:((rows/2)+1), ] #second half, in reverse
```

#### 2. For each rearrangement (permutation), calculate the difference in group means.

``` r
diff_means <- rowMeans(grp1) - rowMeans(grp2)
```

#### 3. See what range 95% of these group means fall into.

``` r
range_95 <- quantile(diff_means, probs = c(0.025, 0.0975))
cat(sprintf("The range of values for 95%% of the difference of means lies between %.2f and %.2f.",
            range_95[1], range_95[2]))
```

    ## The range of values for 95% of the difference of means lies between -17.35 and -12.56.

#### 4. See if the difference in means we observe in the data falls within this 95% range.

``` r
diff_means_obs <- mean(ctrl) - mean(trt)
cat(sprintf("The difference of means between the observed groups is %.2f. This is %s within the 95%% range of the combination groups.",
            diff_means_obs, ifelse( range_95[1] < diff_means_obs && range_95[2] > diff_means_obs, 
                                    yes = "",
                                    no ="not")))
```

    ## The difference of means between the observed groups is -10.33. This is not within the 95% range of the combination groups.

#### 5. Alternatively, to get a p-value, see what the probability of observing a difference of the same or larger magnitude than what we saw in the data

``` r
same_or_larger <- subset(diff_means, abs(diff_means) >= abs(diff_means_obs))

cat(sprintf("The number of difference of means in combinations that are >= than difference of means in observed is %d (ratio of %.2f).\n",
    length(same_or_larger),
    length(same_or_larger)/length(diff_means)))
```

    ## The number of difference of means in combinations that are >= than difference of means in observed is 3 (ratio of 0.30).

``` r
cat(sprintf("From the solutions: sum(same_or_larger)/length(diff_means) = %.2f\n", sum(same_or_larger)/length(diff_means)))
```

    ## From the solutions: sum(same_or_larger)/length(diff_means) = -4.10

#### Next try it with these observations:

##### Control: 110, 97, 98, 100, 103

##### Treatment: 123, 109, 119, 105, 106

``` r
assess_group_combinations <- function(g1, g2) {
  observations <- unlist(c(g1, list(unlist(g2))))
  
  cat(sprintf("Assessment for observations: %s\n\n", toString(observations)))
  
  obsv_comb = combinations(length(observations), 3, observations) 
  rows = nrow(obsv_comb)
  
  # grouping combinations
  grp1 = obsv_comb[1:(rows/2),] #first half of combinations
  grp2 = obsv_comb[rows:((rows/2)+1), ] #second half, in reverse
  
  diff_means <- rowMeans(grp1) - rowMeans(grp2)
  
  range_95 <- quantile(diff_means, probs = c(0.025, 0.0975))
  cat(sprintf("The range of values for 95%% of the difference of means lies between %.2f and %.2f.\n\n",
              range_95[1], range_95[2]))
  
  diff_means_obs <- mean(g1) - mean(g2)
  cat(sprintf("The difference of means between the observed groups is %.2f. This is %s within the 95%% range of the combination groups.\n\n",
              diff_means_obs, ifelse( range_95[1] < diff_means_obs && range_95[2] > diff_means_obs, 
                                      yes = "",
                                      no ="not")))
  
  same_or_larger <- subset(diff_means, abs(diff_means) >= abs(diff_means_obs))
  
  cat(sprintf("The number of difference of means in combinations that are >= than difference of means in observed is %d (ratio of %.2f).\n\n",
      length(same_or_larger),
      length(same_or_larger)/length(diff_means)))
  
  cat(sprintf("From the solutions: sum(same_or_larger)/length(diff_means) = %.2f\n", sum(same_or_larger)/length(diff_means)))
}
```

``` r
assess_group_combinations(c(110, 97, 98, 100, 103), c(123, 109, 119, 105, 106))
```

    ## Assessment for observations: 110, 97, 98, 100, 103, 123, 109, 119, 105, 106
    ## 
    ## The range of values for 95% of the difference of means lies between -16.24 and -11.58.
    ## 
    ## The difference of means between the observed groups is -10.80. This is not within the 95% range of the combination groups.
    ## 
    ## The number of difference of means in combinations that are >= than difference of means in observed is 8 (ratio of 0.13).
    ## 
    ## From the solutions: sum(same_or_larger)/length(diff_means) = -1.92
