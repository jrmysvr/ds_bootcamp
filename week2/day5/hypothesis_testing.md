Exercise: Hypothesis Testing
================

``` r
library(ggplot2)
library(dplyr)
```

##### Load Data

``` r
PATH = paste(getwd(), "data", sep="/")
titanic = read.csv(paste(PATH, "titanic.csv", sep="/"))
plants = read.csv(paste(PATH, "plant_growth.csv", sep="/"))
```

------------------------------------------------------------------------

### Use *titanic.csv* to answer the following questions:

#### Are passenger gender and cabin class related?

The null hypothesis for this question: gender and cabin class are unrelated.

The genders can be group and the resulting number of classes counted between groups. Additionally, a chi-squared test can be performed to check contingency between the two variables.

``` r
ggplot(na.omit(select(titanic, Sex, PClass)), aes(Sex, fill=PClass)) + 
  geom_bar()
```

![](images/unnamed-chunk-3-1.png)

There seems to be a mismatch between the ratios of men/women to each class. There seem to be many more men in 3rd class - maybe those also include laborers?

``` r
class_vs_sex <- chisq.test(titanic$Sex, titanic$PClass)
```

    ## Warning in chisq.test(titanic$Sex, titanic$PClass): Chi-squared
    ## approximation may be incorrect

``` r
print(class_vs_sex)
```

    ## 
    ##  Pearson's Chi-squared test
    ## 
    ## data:  titanic$Sex and titanic$PClass
    ## X-squared = 22.768, df = 3, p-value = 4.514e-05

``` r
cat(sprintf("With a p-value of %1.3e, the null hypothesis is %s.",
            class_vs_sex$p.value, 
            ifelse((class_vs_sex$p.value) >= 0.05, "accepted. The variables are unrelated", "rejected. The variables are related")))
```

    ## With a p-value of 4.514e-05, the null hypothesis is rejected. The variables are related.

#### Does the age of the passengers vary across cabin class?

The null hypothesis in this experiment: The ages between cabin classes do not vary.

``` r
qplot(data = na.omit(select(titanic, PClass, Age)), PClass, Age) +
  geom_boxplot()
```

![](images/unnamed-chunk-5-1.png)

It appears that median age decreases from 1st to 3rd class.

Check normality of the three groups

``` r
check_if_normal <- function(vals, label="") {
  if (length(label) > 0) label = paste(" of", label)
  res <- shapiro.test(vals)
  is_normal <- res$p.value > 0.05
  cat(sprintf("The p-value%s is %1.3e so the distribution is %s.\n",
              label,
              res$p.value,
              ifelse(is_normal, "normal", "not normal")))
  # return(is_normal)
}
```

``` r
first = subset(titanic, PClass == "1st")$Age
second = subset(titanic, PClass == "2nd")$Age
third = subset(titanic, PClass == "3rd")$Age
```

``` r
check_if_normal(first, "1st class")
```

    ## The p-value of 1st class is 2.002e-02 so the distribution is not normal.

``` r
check_if_normal(second, "2nd class")
```

    ## The p-value of 2nd class is 1.806e-03 so the distribution is not normal.

``` r
check_if_normal(third, "3rd class")
```

    ## The p-value of 3rd class is 7.680e-05 so the distribution is not normal.

The distributions are not normal, so F-test cannot be used. Instead, Kruskal-Wallis test is used to check variance between distributions.

``` r
# length of second is less than length of first
first_vs_second <- kruskal.test(sample(first, length(second)), second)
print(first_vs_second)
```

    ## 
    ##  Kruskal-Wallis rank sum test
    ## 
    ## data:  sample(first, length(second)) and second
    ## Kruskal-Wallis chi-squared = 46.902, df = 50, p-value = 0.5985

``` r
cat(sprintf("With a p-value of %1.3e, the null hypothesis can be %s.",
            first_vs_second$p.value,
            ifelse( first_vs_second$p.value >= 0.05, "accepted. The ages do not vary between classes", "rejected. The ages do vary between classes")))
```

    ## With a p-value of 5.985e-01, the null hypothesis can be accepted. The ages do not vary between classes.

``` r
#length of second is less than length of third
second_vs_third <- kruskal.test(second, sample(third, length(second)))
print(second_vs_third)
```

    ## 
    ##  Kruskal-Wallis rank sum test
    ## 
    ## data:  second and sample(third, length(second))
    ## Kruskal-Wallis chi-squared = 42.954, df = 40, p-value = 0.3458

``` r
cat(sprintf("With a p-value of %1.3e, the null hypothesis can be %s.",
            second_vs_third$p.value,
            ifelse( second_vs_third$p.value >= 0.05, "accepted. The ages do not vary between classes", "rejected. The ages do vary between classes")))
```

    ## With a p-value of 3.458e-01, the null hypothesis can be accepted. The ages do not vary between classes.

``` r
#length of first is less than length of third
first_vs_third <- kruskal.test(first, sample(third, length(first)))
print(first_vs_third)
```

    ## 
    ##  Kruskal-Wallis rank sum test
    ## 
    ## data:  first and sample(third, length(first))
    ## Kruskal-Wallis chi-squared = 42.779, df = 37, p-value = 0.2369

``` r
cat(sprintf("With a p-value of %1.3e, the null hypothesis can be %s.",
            first_vs_third$p.value,
            ifelse( first_vs_third$p.value >= 0.05, "accepted. The ages do not vary between classes", "rejected. The ages do vary between classes")))
```

    ## With a p-value of 2.369e-01, the null hypothesis can be accepted. The ages do not vary between classes.

``` r
#length of first is less than length of third
all_classes <- kruskal.test(titanic$Age, titanic$PClass)
 print(all_classes)
```

    ## 
    ##  Kruskal-Wallis rank sum test
    ## 
    ## data:  titanic$Age and titanic$PClass
    ## Kruskal-Wallis chi-squared = 128.03, df = 2, p-value < 2.2e-16

``` r
cat(sprintf("With a p-value of %1.3e, the null hypothesis can be %s.",
            all_classes$p.value,
            ifelse( all_classes$p.value >= 0.05, "accepted. The ages do not vary between classes", "rejected. The ages do vary between classes")))     
```

    ## With a p-value of 1.580e-28, the null hypothesis can be rejected. The ages do vary between classes.

From the results of the Kruskal-wallis tests, the null hypothesis can be accepted.

------------------------------------------------------------------------

##### For each question below:

-   Test necessary assumptions about your data
-   Run the appropriate type of t-test
-   Interpret the result

### Questions:

#### Were the passengers who survived older or younger than those who did not?

#### Were men older than women on average?

------------------------------------------------------------------------

### Use *plant\_growth.csv* to answer the following questions:

#### Did the treaments have an effect?

The null hypothesis for this experiment: The treaments did not have an effect. There is no variance between the distributions.

``` r
ctrl <- subset(plants, plants$group == "ctrl")$weight
trt1 <- subset(plants, plants$group == "trt1")$weight
trt2 <- subset(plants, plants$group == "trt2")$weight

check_if_normal(ctrl, "ctrl")
```

    ## The p-value of ctrl is 7.475e-01 so the distribution is normal.

``` r
check_if_normal(ctrl, "trt1")
```

    ## The p-value of trt1 is 7.475e-01 so the distribution is normal.

``` r
check_if_normal(ctrl, "trt2")
```

    ## The p-value of trt2 is 7.475e-01 so the distribution is normal.

The distributions of the groups are normal, so the F test can be used to compare variances.

``` r
ctrl_vs_trt1 <- var.test(ctrl, trt1)
print(ctrl_vs_trt1)
```

    ## 
    ##  F test to compare two variances
    ## 
    ## data:  ctrl and trt1
    ## F = 0.53974, num df = 9, denom df = 9, p-value = 0.3719
    ## alternative hypothesis: true ratio of variances is not equal to 1
    ## 95 percent confidence interval:
    ##  0.1340645 2.1730025
    ## sample estimates:
    ## ratio of variances 
    ##          0.5397431

``` r
cat(sprintf("With a p-value of %1.3e, the null hypothesis can be %s.",
            ctrl_vs_trt1$p.value,
            ifelse( ctrl_vs_trt1$p.value >= 0.05, "accepted. The treatment does not vary", "rejected. The treatment does vary")))
```

    ## With a p-value of 3.719e-01, the null hypothesis can be accepted. The treatment does not vary.

``` r
ctrl_vs_trt2 <- var.test(ctrl, trt2)
print(ctrl_vs_trt2)
```

    ## 
    ##  F test to compare two variances
    ## 
    ## data:  ctrl and trt2
    ## F = 1.7358, num df = 9, denom df = 9, p-value = 0.4239
    ## alternative hypothesis: true ratio of variances is not equal to 1
    ## 95 percent confidence interval:
    ##  0.4311513 6.9883717
    ## sample estimates:
    ## ratio of variances 
    ##           1.735813

``` r
cat(sprintf("With a p-value of %1.3e, the null hypothesis can be %s.",
            ctrl_vs_trt2$p.value,
            ifelse( ctrl_vs_trt2$p.value >= 0.05," accepted. The treatment does not vary", "rejected. The treatment does vary")))
```

    ## With a p-value of 4.239e-01, the null hypothesis can be  accepted. The treatment does not vary.

From these results, the null hypothesis is accepted: the treatments do not have an effect compared to the control group.

------------------------------------------------------------------------
