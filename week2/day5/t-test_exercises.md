Exercise 2: T-test exercise
================

#### Take two distributions, where either: a) both are the same normal distribution ~N(50,5), b) each is from a different normal distribution ~N(50, 5), ~N(45, 5)

``` r
norm1 <- function(n=30) rnorm(n, 50, 5)
norm2 <- function(n=30) rnorm(n, 45, 5)
```

#### For many (1000) sets of these two distributions, run a t-test for each set of samples. Using this simulation, how often is there a type I or type II error?

``` r
n_sets <- 1000
set_A <- replicate(n_sets, t.test(norm1(), norm1())$p.value)
set_B <- replicate(n_sets, t.test(norm1(), norm2())$p.value)
```

*Type I error - the incorrect rejection of the null hypothesis*

*Type II error - the failure to reject the null hypothesis*

The hypothesis should be rejected if the p-value from the respective statistical test is below the significance level, *α* (commonly, 0.05). This *significance* relates to a confidence level: with 100\*(1-*α*)% confidence, the null hypothesis can be accepted/rejected. There is a 100\**α*% chance that type I error will occur where the null hypothesis is rejected when it should have been accepted - a false positive in relationship between two variables - or type II error where the null hypothesis is not rejected when it should have been - a false negative in no relationship between two variables.

##### how many times was p.value &lt; significance level

``` r
cat(sprintf("In %d t-tests of two identical normal distributions, there were %d occurrances (%.2f%%) of type I or type II error.\n",
            n_sets,
            sum(set_A < 0.05),
            100*mean(set_A < 0.05)))
```

    ## In 1000 t-tests of two identical normal distributions, there were 51 occurrances (5.10%) of type I or type II error.

``` r
cat(sprintf("In %d t-tests of two different normal distributions, there were %d occurrances (%.2f%%) of type I or type II error.",
            n_sets,
            sum(set_B < 0.05),
            100*mean(set_B < 0.05)))
```

    ## In 1000 t-tests of two different normal distributions, there were 966 occurrances (96.60%) of type I or type II error.

*Bonus: What happens if you change the sample sizes or the difference in means? We'll talk about this "power" to detect effects next week.*

``` r
compare_sample_sizes <- function(s1, s2) {
  cat(sprintf("%.2f%% type I or type II error in identical normal distributions of size %d and %d.\n", 
              100*mean(replicate(n_sets, t.test(norm1(s1), norm1(s2))$p.value < 0.05)),
              s1,  s2))
}

compare_sample_means <- function(m1, m2, n=50, sd=5) {
  cat(sprintf("%.2f%% type I or type II error in two normal distributions (n=%d, sd=%d) with means %d and %d.\n", 
              100*mean(replicate(n_sets, t.test(rnorm(n,m1,sd), rnorm(n,m2,sd))$p.value < 0.05)),
              n,sd,m1,m2))
}

compare_sample_std_devs <- function(sd1, sd2, n=50, mean=10) {
  cat(sprintf("%.2f%% type I or type II error in two normal distributions (n=%d, mean=%d) with std. deviations %d and %d.\n", 
              100*mean(replicate(n_sets, t.test(rnorm(n,mean,sd1), rnorm(n,mean,sd2))$p.value < 0.05)),
              n,mean,sd1,sd2))
}
```

``` r
print("\nNormal distributions of varying sample sizes.")
```

    ## [1] "\nNormal distributions of varying sample sizes."

``` r
compare_sample_sizes(1000, 50)
```

    ## 5.00% type I or type II error in identical normal distributions of size 1000 and 50.

``` r
compare_sample_sizes(30, 15)
```

    ## 4.40% type I or type II error in identical normal distributions of size 30 and 15.

``` r
compare_sample_sizes(10000, 10)
```

    ## 4.10% type I or type II error in identical normal distributions of size 10000 and 10.

``` r
print("Normal distributions with varying means.")
```

    ## [1] "Normal distributions with varying means."

``` r
compare_sample_means(50, 45)
```

    ## 99.90% type I or type II error in two normal distributions (n=50, sd=5) with means 50 and 45.

``` r
compare_sample_means(50, 40)
```

    ## 100.00% type I or type II error in two normal distributions (n=50, sd=5) with means 50 and 40.

``` r
compare_sample_means(100, 105)
```

    ## 99.80% type I or type II error in two normal distributions (n=50, sd=5) with means 100 and 105.

``` r
compare_sample_means(10, 11)
```

    ## 18.80% type I or type II error in two normal distributions (n=50, sd=5) with means 10 and 11.

``` r
print("Normal distributions with varying standard deviations")
```

    ## [1] "Normal distributions with varying standard deviations"

``` r
compare_sample_std_devs(10,11)
```

    ## 4.70% type I or type II error in two normal distributions (n=50, mean=10) with std. deviations 10 and 11.

``` r
compare_sample_std_devs(10,20)
```

    ## 5.10% type I or type II error in two normal distributions (n=50, mean=10) with std. deviations 10 and 20.

``` r
compare_sample_std_devs(100,101)
```

    ## 3.50% type I or type II error in two normal distributions (n=50, mean=10) with std. deviations 100 and 101.

------------------------------------------------------------------------
