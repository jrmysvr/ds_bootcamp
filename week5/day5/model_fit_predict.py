
def model_fit_predict(Model, train, test, train_target, test_target, label=None, **kwargs):
    """ Using a sklearn Model, fit and predict values after training on data and target values"""
    
    model = Model(**kwargs)
    preds = model.fit(train, train_target).predict(test)
    
    # Report results
    print(label)
    print("Number of mislabeled points out of {} points: {}".format(len(test), (test_target != preds).sum()))
    
    return model