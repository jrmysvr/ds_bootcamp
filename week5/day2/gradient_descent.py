
import numpy as np

def gradient_descent(X, y, n_iters: int, learning_rate=0.01, break_early=True):
    
    # 1. Define total_error over all points in data
    total_error = np.zeros(n_iters) 
    
    # 2. Initialize b_estimate, m_estimate, and learning_rate
    b_estimate = 0
    m_estimate = 0
    
    # 3. Define step_gradient as
        # a. b_gradient = 0, m_gradient = 0
        # b. loop over all data and obtain values of b_gradient, m_gradient as:
            # b_estimate += learning_rate * b_gradient of i
            # m_estimate += learning_rate * m_gradient of i

    N = len(X)
    for i in range(n_iters):
        # y_est = X.apply(lambda x: m_estimate * x + b_estimate).values
        est_f = np.vectorize(lambda x: (m_estimate * x) + b_estimate)
        y_est = est_f(X)

        est_term =  y_est - y
        
        b_term = est_term
        b_gradient = (2/N)*sum(b_term)

        b_estimate -= (learning_rate*b_gradient)
        
        m_term = X * est_term
        m_gradient = (2/N)*sum(m_term)   

        m_estimate -= (learning_rate*m_gradient)
    
        total_error[i] = (1/N) * sum(est_term**2)
        
        if break_early and i > 0:
            #check error
            threshold = 5e-5
            diff_error = abs(total_error[i-1] - total_error[i])/total_error[i-1]
            if diff_error < threshold:
                total_error = total_error[:i]
                break
        
    return (m_estimate, b_estimate, total_error)