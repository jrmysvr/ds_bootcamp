import pandas as pd
from sklearn.preprocessing import LabelEncoder

def label_encode_columns(df, col_names):
    """ Using 'LabelEncoder', create new dataframe with encoded categorical values 
    
    :param df: pd.DataFrame
    :param col_names: list of column names (str)
    :return: new_df: pd.DataFrame, encoders: dict of LabelEncoders
    """
    encoders = dict()
    new_df = pd.DataFrame()
    
    for col in col_names:
        encoder = LabelEncoder().fit(df[col])
        encoders[col] = encoder
        new_df[col] = encoder.transform(df[col])
        
    return (new_df, encoders)

def label_decode_columns(df, encoder_dict):
    decoded_df = pd.DataFrame(columns=encoder_dict.keys())
    
    for key in encoder_dict.keys():
        if key in df.columns:
            value = df[key]
            decoded_df[key] = encoder_dict[key].inverse_transform(value)
            
    return decoded_df