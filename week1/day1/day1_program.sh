main_loop() {

  format
  
	echo "Hello, $1."
	while true; do
		echo "What would you like to do?"
		echo "=========================="
		echo "1. Read a joke"
		echo "2. Show your disk usage"
		echo "3. Show your current directory"
		echo "4. Exit this program"
		
    format

		read CHOICE
		if ((CHOICE == 1)); then 
			sh  ./tell_a_joke.sh
		elif ((CHOICE == 2)); then
			sh ./show_disk_usage.sh
		elif ((CHOICE == 3)); then
			sh ./current_directory.sh
		elif ((CHOICE == 4)); then
			sh ./good_bye_user.sh $1
			break
		else
			echo "$1, that is not an option..."
			echo "Please, try again"
		fi
  
    format

	done
}

format() {
  echo
  echo
}

read -p "User, what is thy name? " NAME

main_loop $NAME

