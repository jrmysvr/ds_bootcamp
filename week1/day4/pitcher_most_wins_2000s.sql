-- Which Yankees pitcher had the most wins in a season in the 2000's?

select distinct P.playerID, T.name, M.nameFirst, M.nameLast, P.W, P.yearID from Pitching as P
join Master as M
on P.playerID = M.playerID
join Teams as T
on P.teamID = T.teamID
where P.yearID >= 2000
and 
T.name = "New York Yankees"
order by P.W desc
limit 5;
