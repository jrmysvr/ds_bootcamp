--Which player averaged the fewest at bats between home runs in 2002?

select B.playerID, 
		M.nameFirst, 
		M.nameLast, 
		B.AB/B.HR as abhr 
		from Batting as B
join Master as M
on M.playerID = B.playerID
where 
B.yearID = 2002
and
abhr is not null
order by abhr asc
limit 1;