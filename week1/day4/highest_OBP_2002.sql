-- Which player had the highest on-base percentage in 2002

select B.yearID, 
		M.nameFirst, 
		M.nameLast,
		B.H,
		B.BB,
		B.HBP,
		B.AB,
		B.SF,
		(1.0*(B.H + B.BB + B.HBP))/ (B.AB + B.BB + B.HBP + B.SH + B.SF)
		as onBase
		from Batting as B
join Master as M
on M.playerID = B.playerID
where 
B.yearID = 2002
and onBase is not null
and B.H != 0
and B.HBP != 0
and B.AB != 0
and B.BB!= 0
and B.SF != 0
order by onBase desc;