--Which player hit the most home runs in 2002
select B.playerID, B.yearID, HR from Batting as B
where B.yearID = 2002
order by HR desc
limit 1;
