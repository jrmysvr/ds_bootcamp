--Which team spent the most/least money 
--on player salaries in 2002

select * from (
select T.teamID, T.name, S.sumSal from Teams as T
join(
select teamID, sum(salary) as sumSal from Salaries
where yearID = 2002
group by teamID
) as S
on S.teamID = T.teamID
group by T.teamID
order by sumSal DESC
limit 1
)
union
select * from (
select T.teamID, T.name, S.sumSal from Teams as T
join(
select teamID, sum(salary) as sumSal from Salaries
where yearID = 2002
group by teamID
) as S
on S.teamID = T.teamID
group by T.teamID
order by sumSal ASC
limit 1
)

--Highest salary
--select teamID, max(salary) from Salaries
--where yearID = 2002;

--Lowest salary
--select teamID, min(salary) from Salaries
--where yearID = 2002;

--select teamID, salary from Salaries
--where yearID = 2002
--and
--(
--salary = (select max(salary) from Salaries where yearID = 2002)
--or
--salary = (select min(salary) from Salaries where yearID = 2002)
--)
--group by teamID,salary

