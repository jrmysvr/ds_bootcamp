-- In the 2000's, did the Yankees draw more or fewer walks (Base-on-Balls or BB) as the decade went on? --> Fewer from 2000 - 2017

select yearID, name, BB from Teams
where yearID >= 2000
and name = "New York Yankees"
order by yearID;