--Which team spent the most/least money 
--on player salaries in 2002

select teamID, min(salary) from Salaries
union 
select teamID, max(salary) from Salaries;