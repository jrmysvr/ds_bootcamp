import unittest

def alphabet_count(dna):
    alphabet = {letter:0 for letter in "AGCT"}

    for letter in dna:
        alphabet[letter]+=1

    return alphabet

class CountNucleotidesTestCases(unittest.TestCase):

    def test_full(self):
        expected = dict(zip('ACGT', [20, 12, 17, 21]))
        test = "AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC"
        actual = alphabet_count(test)

        self.assertEqual(expected['A'], actual['A'])
        self.assertEqual(expected['C'], actual['C'])
        self.assertEqual(expected['G'], actual['G'])
        self.assertEqual(expected['T'], actual['T'])

if __name__ == "__main__":
    print(alphabet_count("AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC"))
    unittest.main()