from abc import ABC, abstractmethod
from random import random

class Game(ABC):

    @abstractmethod
    def play(self):
        pass

    def desc(self):
        return self.play.__doc__

class ComputerGuess(Game):

    def __init__(self,low,high):
        self.high = high
        self.low = low

    def play(self):
        """
        Play a number game with computer: Computer guesses your number
        """
        low = 1
        high = 10
        print("Computer will guess your number")
        print("\nThink of a number between {} and {}".format(low, high))
        input("  (press any key when ready)  ")

        confirm = ["yes", "y", "Yes", "Y",]
        deny = ["no", "n", "No", "N"]
        index = round(((high-low) + low)/2)
        guess = low + index

        while True:
            response = input("\nIs your number {}?  ".format(guess))
            if response in confirm:
                break
            if response in deny:
                high_low = input("Was the guess too high or too low?  ")
                index = 1 if index == 0 else round(index/2)
                guess += index if high_low == "low" else -index
            else:
                print("I don't understand...\n")

        print("Awesome, {} is a great choice\n\n".format(guess))

class UserGuess(Game):

    def __init__(self,low,high):
        self.high = high
        self.low = low

    def play(self):
        """
        Play a number game with computer: You guess the computer's number
        """
        low = 1
        high = 10
        print("Guess the computer's number")
        print("I'm thinking of a number between {} and {}".format(low, high))
        num = int(random() * (high-low) + low)

        guess = input("What number am I thinking of?")
        count = 1
        while int(guess) != num:
            print("Nope, that's not it.")
            print("Too high" if int(guess) > num else "Too low")
            count += 1
            guess = input("Try again: ")

        print("Well done!")
        print("That only took {} attempts".format(count))

class MainGame(Game):

    def __init__(self, *option_classes):
        self.options = self._gen_options(*option_classes)

    def _gen_options(self, *classes):
        options = dict()
        for i, cls in enumerate(classes):
            options[str(i+1)] = cls

        return options

    def display_options(self):
        for option in sorted(self.options):
            print("{}: {}".format(option, self.options[option].desc()))

    def play(self):
        exit_options = ["exit", "x"]
        while True:
            self.display_options()
            choice = input("What would you like to do?  ")
            if choice in self.options:
                self.options[choice].play()
            elif choice in exit_options:
                break
            else:
                print("That's not a valid option")

        print("So Long...")

if __name__ == "__main__":
    computer_guess = ComputerGuess(1, 10)
    user_guess = UserGuess(1,10)

    main_game = MainGame(computer_guess, user_guess)
    main_game.play()