from random import random

def guess_computer_number():
    """
    Play a number game with computer: You guess the computer's number
    """
    low = 1
    high = 10
    print("Guess the computer's number")
    print("I'm thinking of a number between {} and {}".format(low, high))
    num = int(random() * (high-low) + low)

    guess = input("What number am I thinking of?")
    count = 1
    while int(guess) != num:
        print("Nope, that's not it.")
        print("Too high" if int(guess) > num else "Too low")
        count += 1
        guess = input("Try again: ")

    print("Well done!")
    print("That only took {} attempts".format(count))


def computer_guesses_number():
    """
    Play a number game with computer: Computer guesses your number
    """
    low = 1
    high = 10
    print("Computer will guess your number")
    print("\nThink of a number between {} and {}".format(low, high))
    input("  (press any key when ready)  ")

    confirm = ["yes", "y", "Yes", "Y",]
    deny = ["no", "n", "No", "N"]
    index = round(((high-low) + low)/2)
    guess = low + index

    while True:
        response = input("\nIs your number {}?  ".format(guess))
        if response in confirm:
            break
        if response in deny:
            high_low = input("Was the guess too high or too low?  ")
            index = 1 if index == 0 else round(index/2)
            guess += index if high_low == "low" else -index
        else:
            print("I don't understand...\n")

    print("Awesome, {} is a great choice\n\n".format(guess))

def display_options(option_dict):
    for option in sorted(option_dict):
        print(option, option_dict[option].__doc__)

def main_loop():
    options = dict()
    options["1"] = guess_computer_number
    options["2"] = computer_guesses_number
    exit_options = ["exit", "x"]
    while True:
        display_options(options)
        choice = input("What would you like to do?  ")
        if choice in options:
            options[choice]()
        elif choice in exit_options:
            break
        else:
            print("That's not a valid option")

    print("Program terminating...")


if __name__ == "__main__":
    main_loop()
