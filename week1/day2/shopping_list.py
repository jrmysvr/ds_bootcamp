
class EndProgram(Exception): pass


def pretty(fn):
    formatter = "\n"
    def wrapper(*args):
        print(formatter)
        fn(*args)
        print(formatter)
    return wrapper


class ShoppingApp:

    def __init__(self):
        self.options = self._gen_options()
        self.menu = self._gen_menu()
        self.ignore = ["main_loop", "_gen_options", "__init__"]
        self.cart = list()

    def _gen_menu(self):
        menu = list()
        menu.append(""" Press 'h' for the help menu""")
        menu.append(""" Press 's' to show the items in your list """)
        menu.append(""" Press 'a' to add a new item to the list """)
        menu.append(""" Press 'r' to remove an item from  the list """)
        menu.append(""" Press 'q' to quit """)

        return menu

    def _gen_options(self):
        options = dict()
        options["h"] = self.help_menu
        options["r"] = self.remove_item
        options["s"] = self.show_items
        options["a"] = self.add_item
        options["q"] = self.quit

        return options

    def main_loop(self):
        print("Welcome to the Shopping App")
        self.help_menu()
        try:
            while True:
                choice = input("What would you like to do? ")
                if choice in self.options:
                    self.options[choice]()
                else:
                    print("Unknown option\n\n")

        except EndProgram:
            pass

    @pretty
    def help_menu(self):
        for m in self.menu:
            print(m)

    @pretty
    def show_items(self):
        if len(self.cart) == 0:
            print("There are no items in your cart")
            return

        for i, item in enumerate(self.cart):
            print("{}: {}".format(i+1, item[0].upper() + item[1:]))

    @pretty
    def add_item(self):
        item = input("What would you like to add? ")
        self.cart.append(item.lower())

    @pretty
    def remove_item(self):
        self.show_items()
        item = input("What would you like to remove? ")
        self.cart.remove(item.lower())


    def quit(self):
        print("Good bye")
        raise EndProgram

if __name__ == "__main__":

    app = ShoppingApp()
    app.main_loop()
