import unittest

def transcribe(rna):
    complement = dict(zip("GACT", "GACU"))
    return "".join([complement[i] for i in list(rna)])

class TranscriptionTestCases(unittest.TestCase):

    def test_full(self):
        expected = "GAUGGAACUUGACUACGUAAAUU"
        test = "GATGGAACTTGACTACGTAAATT"
        self.assertEqual(expected, transcribe(test))

if __name__ == "__main__":
    print(transcribe("GATGGAACTTGACTACGTAAATT"))
    unittest.main()