

def main_loop():
    print("####################################")
    print("Let's calculate your BMI (kg/m^2)\n")
    weight = int(input("What is your weight in kilograms?  "))
    height = int(input("What is your height in centimeters?  "))
    print("####################################\n")

    bmi = 10000 * weight / height**2

    underweight = lambda x: x > 0 and x <= 18.5
    normal = lambda x: x > 18.5 and x <= 25
    overweight = lambda x: x > 25 and x <= 30

    if(underweight(bmi)):
        msg = "underweight"
    elif(normal(bmi)):
        msg = "normal"
    elif(overweight(bmi)):
        msg = "overweight"
    else:
        raise Exception("BMI unknown")

    print("Your BMI, {:.2f}, suggests that you are in the {} range.".format(bmi, msg))

if __name__ == "__main__":
    main_loop()