
import unittest
import unittest.mock as mock

import day2_extra

class TransactionTests(unittest.TestCase):

    def mock_input_assert_equal(self, inputs, expected):
        mock_input = mock.MagicMock(side_effect=inputs)
        with mock.patch.object(day2_extra, "input", mock_input):
            actual = day2_extra.transactions()
            self.assertEqual(expected, actual)

    def test_example1(self):
        inputs = [ "D 100", "W 200", None]
        expected = -100
        self.mock_input_assert_equal(inputs, expected)

    def test_example2(self):
        inputs = ["D 300", "D 300", "W 200", "D 100", None]
        expected = 500
        self.mock_input_assert_equal(inputs, expected)


