import unittest
from string_exercises import StringCheckerTests, SpacesDigitsTests, \
                             NumberYoooTests, SortRemoveTests, \
                             CatMouseTests, LetterCounterTests
from list_exercises import ListTupleTests, SameTypeListTests, \
                           ConvertDigitTests, ZeroSumListTests, \
                           PermuteTests
from dict_exercises import CountRepetitionTests, SplitBillTests, \
                           InsaneDictTests, NestceptionTests
from number_exercises import NumberToTextTests, ExponentiationTests
from mock_exercises import TransactionTests

def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(StringCheckerTests))
    suite.addTests(unittest.makeSuite(SpacesDigitsTests))
    suite.addTests(unittest.makeSuite(NumberYoooTests))
    suite.addTests(unittest.makeSuite(SortRemoveTests))
    suite.addTests(unittest.makeSuite(CatMouseTests))
    suite.addTests(unittest.makeSuite(ListTupleTests))
    suite.addTests(unittest.makeSuite(SameTypeListTests))
    suite.addTests(unittest.makeSuite(ConvertDigitTests))
    suite.addTests(unittest.makeSuite(CountRepetitionTests))
    suite.addTests(unittest.makeSuite(SplitBillTests))
    suite.addTests(unittest.makeSuite(NumberToTextTests))
    suite.addTests(unittest.makeSuite(InsaneDictTests))
    suite.addTests(unittest.makeSuite(ExponentiationTests))
    suite.addTests(unittest.makeSuite(ZeroSumListTests))
    suite.addTests(unittest.makeSuite(PermuteTests))
    suite.addTests(unittest.makeSuite(LetterCounterTests))
    suite.addTests(unittest.makeSuite(NestceptionTests))
    suite.addTests(unittest.makeSuite(TransactionTests))
    return suite

if __name__ == "__main__":
   runner = unittest.TextTestRunner()
   runner.run(suite())

