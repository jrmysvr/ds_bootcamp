
import unittest
from itertools import permutations

from day2_extra import is_array_or_tuple, are_same_type, convert, zero_sum, permute


class ListTupleTests(unittest.TestCase):

    def test_string_return_false(self):
        self.assertFalse(is_array_or_tuple("hello"))

    def test_list_return_true(self):
        self.assertTrue(is_array_or_tuple(["hello"]))

    def test_tuple_return_true(self):
        self.assertTrue(is_array_or_tuple(("hello", "asdj`")))

    def test_dict_return_false(self):
        self.assertFalse(is_array_or_tuple({'a':2}))

    def test_set_return_false(self):
        self.assertFalse(is_array_or_tuple(set()))


class SameTypeListTests(unittest.TestCase):

    def test_all_same_return_true(self):
        self.assertTrue(are_same_type(['hello', 'world', 'long sentence']))
        self.assertTrue(are_same_type([1,2,3,4]))
        self.assertTrue(are_same_type([set(), set(list("hello")), set([1,1,1])]))

    def test_all_same_different_level_return_false(self):
        self.assertFalse(are_same_type(['hello', ['hello'], ['bye']]))

    def test_other_type_given_return_false(self):
        self.assertFalse(are_same_type(1))
        self.assertFalse(are_same_type("hello"))


class ConvertDigitTests(unittest.TestCase):

    def test_example1(self):
        self.assertEqual([4,3,2], convert(324))

    def test_example2(self):
        self.assertEqual([9,6,5,4,3,2], convert(429563))


class ZeroSumListTests(unittest.TestCase):

    def test_example1(self):
        test = [1,-1]
        expected = [[0,1]]
        self.assertListEqual(expected, zero_sum(test))

    def test_example2(self):
        test = [0, 4, 3, 5]
        expected = [[0,0]]
        self.assertListEqual(expected, zero_sum(test))

    def test_example3(self):
        test = [1, 5, 0, -5, 3, -1]
        expected = [[0, 5], [1, 3], [2, 2]]
        self.assertListEqual(expected, zero_sum(test))


class PermuteTests(unittest.TestCase):

    def test_example1(self ):
        test = [1, 2, 3]
        expected = [[3, 2, 1], [2, 3, 1], [2, 1, 3], [3, 1, 2], [1, 3, 2], [1, 2, 3]]
        self.assertListEqual(expected, permute(test))

    def test_2(self):
        test = [1,2,3,4,5,6,7,8]
        actual = permute(test)
        expected = list(permutations(test))
        for each in expected:
            self.assertIn(list(each), actual)
