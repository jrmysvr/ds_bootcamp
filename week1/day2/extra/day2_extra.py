from random import random


###################################################

def is_string(string):
    return type(string) == str

def is_only_string(string):
    nums = "1234567890"
    if is_string(string):
        for i in string:
            if i in nums or i == ' ':
                return False

        return True

    return False

def is_alphanumeric(string):
    nums = "1234567890"
    decimal = False
    if is_string(string) and len(string) > 0:
        for i in string:
            if i is '.' and not decimal:
                decimal = True
            elif i not in nums:
                return False

        return True

    return False

def intersection(string1, string2):
    return "".join(sorted(set(string1 + string2)))

def is_caught(scenario, distance=3):
    cat = scenario.index("C")
    mouse = scenario.index("m")
    return abs(cat - mouse) <= distance

def letter_counter(string):
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    upper_count = 0
    lower_count = 0
    for s in string:
        if s in alphabet:
            lower_count += 1
        elif s.lower() in alphabet:
            upper_count += 1

    print("UPPER CASE {}".format(upper_count))
    print("LOWER CASE {}".format(lower_count))
    return (upper_count, lower_count)

def transactions():
    balance = 0

    line = input()
    while line is not None:
        operation, amount = line.split(" ")
        amount = float(amount)

        balance += amount if operation == "D" else -amount
        line = input()
    return balance

###################################################

def is_array_or_tuple(thing):
    if type(thing) == list or type(thing) == tuple:
        return True

    return False

def are_same_type(arr):
    if is_array_or_tuple(arr):
        return len(set([type(thing) for thing in arr])) == 1

    return False

def convert(num):
    n = int(num)
    digits = list()
    while n > 0:
        digits.append(round(10 * ((n/10)-int(n/10))))
        n = int(n/10)
    return sorted(digits, reverse=True)

    #return [int(i) for i in sorted(list(str(num)), reverse=True)]

def zero_sum(num_list):
    output = list()
    for i in range(len(num_list)):
        for j in range(i, len(num_list)):
            if num_list[i] + num_list[j] == 0:
                output.append([i, j])
    return output


def permute(arr):
    pass

###################################################

def count_repetitions(items):
    output = dict()
    for item in items:
        if item in output:
            output[item] += 1
        else:
            output[item] = 1

    return output

def split_the_bill(people):

    bill = 0
    for name in people:
        bill += people[name]

    bill /= len(people)
    return {name:(bill-people[name]) for name in people}

def nt_dictionary(num):
    output = dict()
    low = 1
    high = 1000
    while num > 0:
        num -= 1
        rand_key = random() * (high-low) + low
        output[rand_key] = rand_key**2

    return output

def new_dict(arr):
    if len(arr) == 0:
        return dict()
    return dict([(arr[0], new_dict(arr[1:]))])

###################################################

def write_number(num):
    # Supports up to 999
    num_dict = dict()
    num_dict[1] = "one"
    num_dict[2] = "two"
    num_dict[3] = "three"
    num_dict[4] = "four"
    num_dict[5] = "five"
    num_dict[6] = "six"
    num_dict[7] = "seven"
    num_dict[8] = "eight"
    num_dict[9] = "nine"
    num_dict[10] = "ten"
    num_dict[11] = "eleven"
    num_dict[12] = "twelve"
    num_dict[13] = "thirteen"
    num_dict[14] = "fourteen"
    num_dict[15] = "fifteen"
    num_dict[16] = "sixteen"
    num_dict[17] = "seventeen"
    num_dict[18] = "eighteen"
    num_dict[19] = "nineteen"
    num_dict[20] = "twenty"
    num_dict[30] = "thirty"
    num_dict[40] = "forty"
    num_dict[50] = "fifty"
    num_dict[60] = "sixty"
    num_dict[70] = "seventy"
    num_dict[80] = "eighty"
    num_dict[90] = "ninety"

    if num < 20:
        return num_dict[num]
    if num < 100:
        n = int(num)
        while n > 10:
            n = int(n/10)
        n *= 10
        return "{}-{}".format(num_dict[n], write_number(num-n))
    else:
        n = int(num/100)
        return "{}-hundred-{}".format(num_dict[n], write_number(num-(n*100)))

def exp(base, exponent):

#    num = base
#    while exponent > 1:
#        exponent -= 1
#        num *= base
#
#    return num

    if exponent < 1:
        return 1
    return base * exp(base, exponent-1) if exponent > 1 else base

