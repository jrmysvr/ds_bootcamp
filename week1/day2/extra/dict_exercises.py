
import unittest

from day2_extra import count_repetitions, split_the_bill, nt_dictionary, new_dict

class CountRepetitionTests(unittest.TestCase):

    def test_example1(self):
        names = ['kerouac', 'fante', 'fante', 'buk', 'hemingway', 'hornby', 'kerouac', 'buk', 'fante']
        actual = count_repetitions(names)
        expected = {'kerouac': 2, 'fante': 3, 'buk': 2, 'hemingway': 1, 'hornby': 1}
        for name in names:
            self.assertEqual(expected[name], actual[name])


class SplitBillTests(unittest.TestCase):

    def test_example1(self):
        group = {
            'Amy': 20,
            'Bill': 15,
            'Chris': 10
        }

        actual = split_the_bill(group)
        expected = { 'Amy': -5, 'Bill': 0, 'Chris': 5 }

        self.assertDictEqual(expected, actual)


class InsaneDictTests(unittest.TestCase):

    def test_example1(self):
        n = 20
        self.assertEqual(len(nt_dictionary(n)), n)

    def test_value_is_key_squared(self):
        n = 50
        test = nt_dictionary(n)
        test_key = list(test.keys())[n-1]
        self.assertEqual(test[test_key], test_key**2)


class NestceptionTests(unittest.TestCase):

   def test_example1(self):
       expected = {1: {2: {3: {4: {5: {}}}}}}
       test = [1,2,3,4,5]
       self.assertDictEqual(expected, new_dict(test))
