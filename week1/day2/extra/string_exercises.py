import unittest

from day2_extra import is_string, is_only_string, is_alphanumeric, intersection, is_caught, letter_counter

class StringCheckerTests(unittest.TestCase):

    def test_string_return_true(self):
        self.assertTrue(is_string("hello"))
        self.assertTrue(is_string('hello'))

    def test_list_with_string_return_false(self):
        self.assertFalse(is_string(["hello"]))

    def test_long_string_return_true(self):
        self.assertTrue(is_string("this is a long sentence"))

    def test_dict_with_strings_return_false(self):
        self.assertFalse(is_string({"a":2}))
        self.assertFalse(is_string({2:"a"}))
        self.assertFalse(is_string({"2":"a"}))


class SpacesDigitsTests(unittest.TestCase):

    def test_no_spaces_digits_return_true(self):
        self.assertTrue(is_only_string("hello"))

    def test_string_digits_return_false(self):
        self.assertFalse(is_only_string("11"))

    def test_list_with_string_return_false(self):
        self.assertFalse(is_only_string(["hello"]))

    def test_long_string_with_spaces_return_false(self):
        self.assertFalse(is_only_string("this is a long sentence"))

    def test_dict_with_strings_return_false(self):
        self.assertFalse(is_only_string({"a":2}))
        self.assertFalse(is_only_string({2:"a"}))
        self.assertFalse(is_only_string({"2":"a"}))


class NumberYoooTests(unittest.TestCase):

    def test_string_empty_return_false(self):
        self.assertFalse(is_alphanumeric(""))

    def test_string_numeric_returns_true(self):
        self.assertTrue(is_alphanumeric("11"))

    def test_list_with_string_return_false(self):
        self.assertFalse(is_alphanumeric(["hello"]))

    def test_long_string_with_spaces_return_false(self):
        self.assertFalse(is_alphanumeric("this is a long sentence"))

    def test_dict_with_strings_return_false(self):
        self.assertFalse(is_alphanumeric({"a":2}))
        self.assertFalse(is_alphanumeric({2:"a"}))
        self.assertFalse(is_alphanumeric({"2":"a"}))

    def test_string_float_return_true(self):
        self.assertTrue(is_alphanumeric("3.14"))

    def test_string_multiple_decimals_return_false(self):
        self.assertFalse(is_alphanumeric("3.14."))


class SortRemoveTests(unittest.TestCase):

    def test_example1(self):
        a = 'xyaabbbccccdefww'
        b = 'xxxxyyyyabklmopq'
        self.assertEqual(intersection(a, b), "abcdefklmopqwxy")

    def test_example2(self):
        a = 'xyaabbbccccdefww'
        x = 'abcdefghijklmnopqrstuvwxyz'
        self.assertEqual(intersection(a, x), "abcdefghijklmnopqrstuvwxyz")


class CatMouseTests(unittest.TestCase):

    def test_longer_than_three_return_false(self):
        self.assertFalse(is_caught("C.....m"))

    def test_equal_to_three_return_true(self):
        self.assertTrue(is_caught("C..m"))

    def test_shorter_than_three_return_true(self):
        self.assertTrue(is_caught("C.m"))

    def test_shorter_than_three_needs_trim(self):
        self.assertTrue(is_caught("...C..m"))

    def test_longer_than_three_needs_trim(self):
        self.assertFalse(is_caught("...C...m"))

    def test_equal_to_three_swapped_return_true(self):
        self.assertTrue(is_caught("m..C"))


class LetterCounterTests(unittest.TestCase):

    def test_Hello_World_excl(self):
        self.assertTupleEqual((2, 8), letter_counter("Hello World!"))

    def test_HELLO_WORLD(self):
        self.assertTupleEqual((10,0), letter_counter("HELLO WORLD"))

    def test_hello_world(self):
        self.assertTupleEqual((0,10), letter_counter("hello world"))

