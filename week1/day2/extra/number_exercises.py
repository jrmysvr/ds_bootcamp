
import unittest

from day2_extra import write_number, exp

class NumberToTextTests(unittest.TestCase):

    def test_eleven(self):
        self.assertEqual("eleven", write_number(11))

    def test_two(self):
        self.assertEqual("two", write_number(2))

    def test_thirty_two(self):
        self.assertEqual("thirty-two", write_number(32))

    def test_ninety_nine(self):
        self.assertEqual("ninety-nine", write_number(99))

    def test_one_hundred_one(self):
        self.assertEqual("one-hundred-one", write_number(101))

    def test_five_hundred_sixty_four(self):
        self.assertEqual("five-hundred-sixty-four", write_number(564))

    def test_nine_hundred_ninety_nine(self):
        self.assertEqual("nine-hundred-ninety-nine", write_number(999))

class ExponentiationTests(unittest.TestCase):

    def test_1_5_return_1(self):
        self.assertEqual(exp(1, 5), 1)

    def test_5_3_return_125(self):
        self.assertEqual(exp(5, 3), 125)

    def test_num_0_return_1(self):
        self.assertEqual(exp(10, 0), 1)
