import os
import pandas as pd
import sqlite3
import json
import requests
import sys
from datetime import datetime

class JobsCHScraper:

    base = "https://www.jobs.ch/api/v1/public/search"

    def _build_jobs_search(self, term, page=1, region=2):
        term = term.replace(" ", "+")
        return "{}?page={}&rows=1&region={}&query={}".format(self.base, page, region, term)

    def _row_builder(self, columns, val_dict):
        output = []
        for c in columns:
            output.append(val_dict[c])
        
        return output

    def _print_progress(self, count, final_count):
        i = int(100 * (count/final_count))
        print("="*i + '>' + "-"*(100 - i), end="\r", flush=True)

    def _format_date(self, date):
        return date.strftime("%Y/%m/%d")

    def _try_key(self, dictionary, key):
        if key in dictionary:
            return dictionary[key]
        return None

    def crawl_api_tabled(self, keyword, table_header):
        job_info = lambda pg: pg.json()['documents'][0]
        num_pages = lambda pg: pg.json()['num_pages']
        current_page = lambda pg: pg.json()['current_page']
        
        initial_search = self._build_jobs_search(keyword)
        page = requests.get(initial_search)
        
        jobs_list = []
        
        while current_page(page) <= num_pages(page):
            info = job_info(page)
            row_vals = dict(zip(table_header, [None for _ in table_header]))
            
            row_vals["Keyword"] = keyword
    #         row_vals["Score"] = count
            row_vals["Content"] = self._try_key(info, 'preview')
            row_vals["Company"] = self._try_key(info, 'company_name')
            if self._try_key(info, 'title') is not None:
                row_vals["Title"] = self._try_key(info, 'title').replace('<em>', '').replace('</em>', '')
            if self._try_key(info, 'initial_publication_date') is not None:
                row_vals["Posting_Date"] = self._try_key(info, 'initial_publication_date').replace('-', '/')
            row_vals["Scrape_Date"] = self._format_date(datetime.today())
            row_vals["Location"] = self._try_key(info, 'place')
        
            new_row = self._row_builder(table_header, row_vals)
            jobs_list.append(new_row)
            new_search = self._build_jobs_search(keyword, page=current_page(page) + 1)
            page = requests.get(new_search)
            self._print_progress(current_page(page), num_pages(page))
            
        return pd.DataFrame(jobs_list, columns=table_header) 

    def crawl_api(self, keyword):
        job_info = lambda pg: pg.json()['documents'][0]
        num_pages = lambda pg: pg.json()['num_pages']
        current_page = lambda pg: pg.json()['current_page']
        
        initial_search = self._build_jobs_search(keyword)
        page = requests.get(initial_search)
        
        jobs_list = []
        
        table_header = [ 'age',
                        'slug',
                        'company_slug',
                        'job_id',
                        'title',
                        'preview',
                        'company_name',
                        'company_id',
                        'publication_date',
                        'initial_publication_date',
                        'place',
                        'company_logo_file',
                        'company_segmentation',
                        'source_platform_id',
                        'offer_id',
                        'is_paid']
        
        special = ['title', 'initial_publication_date']
        
        while current_page(page) <= num_pages(page):
            info = job_info(page)
            row_vals = dict(zip(table_header, [None for _ in table_header]))
        
            for col in table_header:
                if col in special:
                    if col == 'title' and self._try_key(info, 'title') is not None:
                        row_vals[col] = self._try_key(info, 'title').replace('<em>', '').replace('</em>', '')
                    if col == 'initial_publication_date':
                        row_vals[col] = self._try_key(info, 'initial_publication_date').replace('-', '/')
                else:
                    row_vals[col] = self._try_key(info, col)
                
            row_vals["Scrape_Date"] = format_date(datetime.today())
            
            new_row = self._row_builder(table_header, row_vals)
            jobs_list.append(new_row)
            new_search = self._build_jobs_search(keyword, page=current_page(page) + 1)
            page = requests.get(new_search)
            self._print_progress(current_page(page), num_pages(page))
            
        return (jobs_list, table_header)

    def save_to_db(self, df, db_path, table_name, if_exists="replace"):
        with sqlite3.connect(db_path, detect_types=sqlite3.PARSE_DECLTYPES) as conn:
            df.to_sql(table_name, conn, if_exists=if_exists)
            conn.commit()


if __name__ == "__main__":
    keywords = ["Data Analyst", "Data Engineer"]
    table_header = ["Keyword", "Score", "Title", "Company", "Content", "Location", "Posting_Date", "Scrape_Date"]
    DB_PATH = os.getcwd() + "/data/" + "jobs_api.sqlite"

    scraper = JobsCHScraper()
    for keyword in keywords:
        results_df = scraper.crawl_api_tabled(keyword, table_header)
        scraper.save_to_db(results_df, DB_PATH, "Jobs", if_exists="append")
        print(results_df.head(5))