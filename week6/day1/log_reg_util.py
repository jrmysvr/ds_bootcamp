from math import exp
from sklearn.metrics import log_loss
import numpy as  np


def softmax(vec):
    """ Scaling a vector using 'softmax'
    
    All values of the scaled vector should sum to one
    :param vec: list of numbers
    :return: scaled list of numbers
    """
    
#     vec_sum = sum(vec)
#     return np.array([i/vec_sum for i in vec])
    vec_exp = np.exp(vec)
    return vec_exp/np.sum(vec_exp)

def multi_softmax(multi_vec):
    """ Scaling a multidimensional vector using 'softmax'
    
    All lists of the scaled multi_vec should sum to one
    :param multi_vec: list of list of numbers
    :return: scaled multi_vec
    """
    
    return np.array([softmax(vec) for vec in multi_vec])

def one_hot(targets_, target_names_):
    encode = lambda x: [1 if i == x else 0 for i in range(len(target_names_))]
    
    return list(map(encode, targets_))

def one_cold(hot):
    return np.apply_along_axis(np.argmax, 1, hot)

def gradient_descent(X_mat, y_mat, learning_rate, break_early=True, threshold = 5e-5, report=False):
    n_observations = len(X_mat)
    n_classes = y_mat.shape[1]
    n_features = len(X_mat[1])
    
    total_error = np.zeros((n_observations, y_mat.shape[1]))
    
    W_estimate = np.zeros((n_features, y_mat.shape[1]))
    b_estimate = np.zeros(y_mat.shape[1])
   
    for i, (x, y) in enumerate(zip(X_mat, y_mat)):
        y_est = np.add(np.dot(x, W_estimate), b_estimate)
        est_term =  np.subtract(y_est, y)

        b_gradient = est_term
        W_gradient = np.outer(x, est_term)

        b_estimate = np.subtract(b_estimate, learning_rate*b_gradient)
        W_estimate = np.subtract(W_estimate, learning_rate*W_gradient)

        total_error[i] = np.mean((est_term)**2)
        
        if report and (i+1) % 100 == 0:
            report_epoch(X_mat[:i], y_mat[:i], W_estimate, b_estimate, epoch=i+1)

    return (W_estimate, b_estimate, total_error)

def predict(X_, W_, b_):
    if len(X_.shape) == 1:
        res = np.add(np.dot(X_, W_), b_)
        return np.argmax(softmax(res))
    else:
        res = np.add(np.dot(X_, W_), b_)
        return np.argmax(multi_softmax(res), axis=1)
    
def accuracy(y_hot, hot_predictions):
    return np.sum(y_hot * hot_predictions)/np.sum(y_hot.T)


def nll(y_true, y_pred, **kwargs):
    """ Apply 'Negative Log Likelihood' to true values and predictions
    
    - Confident yet incorrect prediction is a high positive number
    - Nearly perfect prediction is close to 0
    
    :param Y_true: list of ground truth values
    :param Y_pred: list of predicted values
    :return (float) average negative log likelihood
    """
    return log_loss(y_true, y_pred, **kwargs)