<p align="right">
<img src="./propulsion.jpg" height="100">
</p>
<h1 align="center" style="margin:0; padding:0;">
 Data Science Bootcamp - Class work
</h1>


This repository contains code, notes, and solutions to exercises from the [data science bootcamp](https://propulsionacademy.com/data-science/) at [Propulsion Academy](https://propulsionacademy.com/).

---

[Week 1](week1)

- [Day 1 - Git and Bash](week1/day1)
	- [Hackerrank: text processing](https://www.hackerrank.com/domains/shell?filters%5Bsubdomains%5D%5B%5D=textpro)
- [Day 2 - Python](week1/day2)
- [Day 3 - Numpy & Pandas](week1/day3)
- [Day 4 - SQL](week1/day4)
- [Day 5 - Web Scraping](week1/day5)

[Week 2](week2)

- [Day 1 - APIs](week2/day1)
- [Day 2 - Independent Data Analysis](week2/day2)
- [Day 3 - Descriptive Statistics](week2/day3)
- [Day 4 - Basic Probability calculus](week2/day4)
- [Day 5 - Inductive Statistics](week2/day5)

[Week 3](week3)

- [Day 1 - Regression Analysis](week3/day1)
- [Day 2 - Inference Schools](week3/day2)
- [Days 3 & 4 - Data Challenge](week3/day3.4)
- [Day 5 - Independent Data Analysis - continued](week3/day5)

[Week 4](week4)

- [Day 1 - Introduction to Visualization](week4/day1)
- [Day 2 - Mapping](week4/day2)
- [Day 3 - Design](week4/day3)
- [Day 4 - Storytelling](week4/day4)
- [Day 5 - Web Interactive](week4/day5)

[Week 5](week5)

- [Day 1 - Introduction to Machine Learning](week5/day1)
- [Day 2 - Optimization and Regression](week5/day2)
- [Day 3 - Logistic Regression](week5/day3)
- [Day 4 - Classifiers](week5/day4)
- [Day 5 - Classifiers part 2](week5/day5)

[Week 6](week6)

- [Day 1 - Introduction to Neural Networks](week6/day1)
- [Day 2 - Supervised Learning Challenge](week6/day2)
- [Day 3 - Unsupervised Learning](week6/day3)
- [Day 4 & 5 - Unsupervised Learning Challenge](week6/day4.5)

[Weeks 7 & 8](week7.8)

- [Day 1 & 2 - NLP](week7.8/day1.2)
- [Day 3 & 4 - NLP and Deep Learning](week7.8/day3.4)
- [Day 5 & 6 - Convolutional Neural Networks](week7.8/day5.6)
- [Day 7 & 8 - Evolutionary Algorithms](week7.8/day7.8)
- [Day 9 & 10 - More Tensorflow](week7.8/day9.10)