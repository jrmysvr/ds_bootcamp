import json

def hide_code_output(fname: str) -> None:
    """
    Replace all 'code' cells from json object with new 'metadata'
    :param fname: should be json file with structure of .ipynb
    :return None
    """

    META = {'hideCode':True, 'hidePrompt':True}

    f = open(fname)
    z = json.load(f)
    f.close()

    for cell in z['cells']:
        if cell['cell_type'] == 'code':
            cell['metadata'] = META 
            
    name, ftype = fname.split('.')
    new_name = "{}{}.{}".format(name,"_hidden_code",ftype)

    f = open(new_name, 'w')
    json.dump(z, f)
    f.close()

if __name__ == "__main__":
    pass
#    fname = "zurich_weather.ipynb"
#    
#    f = open(fname)
#    z = json.load(f)
#    f.close()
#
#    meta = {'hideCode':True, 'hidePrompt':True}
#    for cell in z['cells']:
#        if cell['cell_type'] == 'code':
#            cell['metadata'] = meta
#            
#
#    name, ftype = fname.split('.')
#
#    new_name = name + "_hidden_code" + ftype
#
#    f = open(new_name, 'w')
#    json.dump(z, f)
#    f.close()