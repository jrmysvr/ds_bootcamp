![bg 150%](images/timeseries_blank.png)

Time Series Analysis 
Money Market Data - 1TGT
===
---

![](images/timeseries_1TGT.png)

---

<div style="text-align: right">
Oil Price Shock - 1973
</div>

![bg](images/timeseries_oil.png)


---

![](images/timeseries_oil.png)
The positive trend before 1973 is immediately reversed following the 1973 oil crisis. 

---