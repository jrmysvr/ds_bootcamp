Regression notebook
================

``` r
library(ggplot2)
library(car)
library(gclus)
library(lattice)
```

Exercise 1: Linear Regression
-----------------------------

### *Are a person's brain size and body size predictive of his or her intelligence?*

### Some researchers (Willerman, et al, 1991) collected the following data (iqsize.txt) on a sample of n = 38 college students:

    Response (y): Performance IQ scores (PIQ) from the revised Wechsler Adult Intelligence Scale. This variable served as the investigator's measure of the individual's intelligence.

    Potential predictor (x1): Brain size based on the count obtained from MRI scans (given as count/10000).

    Potential predictor (x2): Height in inches.

    Potential predictor (x3): Weight in pounds.

``` r
iqsize <- read.csv("data/iqsize.txt", sep="\t", header=T)
head(iqsize, 5)
```

    ##   PIQ  Brain Height Weight
    ## 1 124  81.69   64.5    118
    ## 2 150 103.84   73.3    143
    ## 3 128  96.54   68.8    172
    ## 4 134  95.15   65.0    147
    ## 5 110  92.88   69.0    146

#### Scale data - column by column

``` r
scale_vector <- function(vec) {
  return(vec/max(vec))
}
```

``` r
scaled_iqsize <- data.frame(sapply(iqsize, FUN=scale_vector))
```

#### Visualize the data appropriately (Hint: use the function plot() or pairs(), use the help-function, to find out how to implement it).

``` r
# plot(iqsize)
pairs(scaled_iqsize,
      main="Scatterplot - Brain Data vs. PIQ")
```

![](images/unnamed-chunk-5-1.png)

``` r
cor(scaled_iqsize)
```

    ##                 PIQ     Brain      Height      Weight
    ## PIQ     1.000000000 0.3778155 -0.09315559 0.002512154
    ## Brain   0.377815463 1.0000000  0.58836684 0.513486971
    ## Height -0.093155590 0.5883668  1.00000000 0.699614004
    ## Weight  0.002512154 0.5134870  0.69961400 1.000000000

\_For more advance scatter plots, review <https://www.statmethods.net/graphs/scatterplot.html_>

``` r
# https://www.rdocumentation.org/packages/car/versions/0.8-1/topics/scatterplot.matrix
spm(~Brain+Height+Weight+PIQ, data=scaled_iqsize,
                   main="Brain Measurements and PIQ")
```

![](images/unnamed-chunk-7-1.png)

#### Describe the output from (a). What appears to be a good predictor?

The variable that coorelates best with `PIQ` is `Brain`, the MRI measurements with a correlation of 0.37. The other two variables show negative and/or nearly zero correlation.

#### Write down the model equation.

The linear regression model:

*y<sub>i</sub>* = *β*<sub>0</sub> + *β*<sub>1</sub>x<sub>i1</sub> + *β*<sub>p</sub>x<sub>ip</sub> + *ϵ*<sub>i</sub>, *i* = 1,..., *n*

also, written as *y<sub>i</sub>* = x<sub>i</sub><sup>T</sup>*β* + *ϵ*<sub>i</sub>

or in matrix notation *y* = X*β* + *ϵ*

#### Implement the model in R.

``` r
iqmodel <- lm(PIQ ~ Brain + Height + Weight, data=scaled_iqsize)

term_info <- iqmodel$terms

cat(sprintf("The linear model's coefficients: %s",
            toString(paste(attr(term_info, "term.labels"),iqmodel$coefficients[2:length(iqmodel$coefficients)]))))
```

    ## The linear model's coefficients: Brain 1.48277730555173, Height -1.40239030339488, Weight 0.000716719522147174

``` r
#Alternate for scaling and creating linear model

box <- bcPower(iqsize, rep(0.1,length(iqsize)))
colnames(box) <- colnames(iqsize)

lm(PIQ ~ Brain + Height + Weight, data=box)
```

    ## 
    ## Call:
    ## lm(formula = PIQ ~ Brain + Height + Weight, data = box)
    ## 
    ## Coefficients:
    ## (Intercept)        Brain       Height       Weight  
    ##     5.55610      1.81154     -1.90178      0.01883

#### Interpret the model output.

Based on the linear model's coefficients, the dominant variable is `Brain`. Also, `Height` has significant influence but negatively. The `Weight` variable hardly affects the model.

------------------------------------------------------------------------

Exercise 2: Multiple Linear Regression
--------------------------------------

### Problem Statement: The Central Bank prints paper money each year. For each year they need an estimate of how much money to be printed. The decision is based on various economic indicators like GDP, Interest rate etc. We will try to model the solution to this problem using Multiple linear regression based on the data (demandmoney.csv).

``` r
demand <- read.csv("data/demandmoney.csv")
cols <- colnames(demand)
cols[1] <- "year"
cols[2] <- "printed"
cols[4] <- "interest"
colnames(demand) <- cols
```

``` r
head(demand, 5)
```

    ##   year printed    GDP interest  WPI
    ## 1 1970    7374 474131     7.25 14.3
    ## 2 1971    8323 478918     7.25 15.1
    ## 3 1972    9700 477392     7.25 16.7
    ## 4 1973   11200 499120     7.25 20.1
    ## 5 1974   11975 504914     9.00 25.1

#### Get insight into the data by using the R-function str().

``` r
str(demand)
```

    ## 'data.frame':    35 obs. of  5 variables:
    ##  $ year    : int  1970 1971 1972 1973 1974 1975 1976 1977 1978 1979 ...
    ##  $ printed : int  7374 8323 9700 11200 11975 13325 16024 14388 17292 20000 ...
    ##  $ GDP     : int  474131 478918 477392 499120 504914 550379 557258 598885 631839 598974 ...
    ##  $ interest: num  7.25 7.25 7.25 7.25 9 10 10 9 9 10 ...
    ##  $ WPI     : num  14.3 15.1 16.7 20.1 25.1 24.8 25.3 26.6 26.6 31.2 ...

#### Summarize the variables separately. Which approach would you use (there are several)?

``` r
sapply(demand, FUN=summary)
```

    ##           year  printed       GDP  interest       WPI
    ## Min.    1970.0   7374.0  474131.0  5.375000  14.30000
    ## 1st Qu. 1978.5  18646.0  615406.5  9.000000  28.90000
    ## Median  1987.0  58555.0  880267.0 10.000000  58.20000
    ## Mean    1987.0 146641.8 1079313.5  9.785714  77.37143
    ## 3rd Qu. 1995.5 227725.0 1452676.0 11.000000 124.40000
    ## Max.    2004.0 647495.0 2389660.0 13.000000 187.30000

#### Identify the explanatory variables and the response variable.

The problem statement calls for a relationship between economic indicators and money to be printed. The explanatory variables are `GDP`, `interest`, and `WPI`. The reponse variable is `printed`.

#### Compute a scatter plot of the variables. Describe how the covariates seem to be associated with the explanatory variable.

``` r
spm(~ GDP + interest + WPI, data=demand,
      main="Explanatory Variables")
```

![](images/unnamed-chunk-14-1.png)

``` r
cor(demand)
```

    ##               year    printed        GDP    interest         WPI
    ## year     1.0000000  0.8743168  0.9552985  0.11660695  0.96849427
    ## printed  0.8743168  1.0000000  0.9762127 -0.29912041  0.95464058
    ## GDP      0.9552985  0.9762127  1.0000000 -0.12509421  0.99323920
    ## interest 0.1166070 -0.2991204 -0.1250942  1.00000000 -0.04066465
    ## WPI      0.9684943  0.9546406  0.9932392 -0.04066465  1.00000000

``` r
demand.corr <- abs(cor(demand))
demand.colors <- dmat.color(demand.corr)
demand.o <- order.single(demand.corr)

cpairs(demand, demand.o, panel.colors = demand.colors, gap=0.5,
       main="Demand data ordered and colored by correlation")
```

![](images/unnamed-chunk-16-1.png) Two of the explanatory variables, `GDP` and `WPI` are highly correlated with `printed`. The variable `interest` is not strongly correlated but might still have an effect.

The variables `year` and `printed` are strongly correlated, but `year` is not an explanatory variable.

``` r
qplot(year, printed, data = demand, geom="line",
      main="Printed money per year") +
  geom_point()
```

![](images/unnamed-chunk-17-1.png)

`GDP` and `WPI` are also correlated, so the model will only utilize `GDP`.

#### Construct a Linear model using R.

``` r
scaled_demand <- data.frame(sapply(demand, FUN = scale_vector))
demand_model <- lm(printed ~ GDP + interest, data=scaled_demand)
```

``` r
summary(demand_model)
```

    ## 
    ## Call:
    ## lm(formula = printed ~ GDP + interest, data = scaled_demand)
    ## 
    ## Residuals:
    ##       Min        1Q    Median        3Q       Max 
    ## -0.072672 -0.011071  0.002212  0.023163  0.071055 
    ## 
    ## Coefficients:
    ##             Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept) -0.02944    0.03385  -0.870    0.391    
    ## GDP          1.10843    0.02551  43.443  < 2e-16 ***
    ## interest    -0.32510    0.03969  -8.191 2.35e-09 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.03462 on 32 degrees of freedom
    ## Multiple R-squared:  0.9848, Adjusted R-squared:  0.9839 
    ## F-statistic:  1038 on 2 and 32 DF,  p-value: < 2.2e-16

#### Interpret the model output. Write down the model equation with the estimated parameters.

The model formula:

*y* = -0.029 + 1.101x<sub>GDP</sub> - 0.325x<sub>interest</sub> + 0.035

``` r
# https://stackoverflow.com/questions/7549694/adding-regression-line-equation-and-r2-on-graph
ggplot(aes(x=printed, y=GDP + interest), data=scaled_demand) +
  geom_smooth(method="lm", formula= y ~ x) +
  geom_point() +
  labs(title="Linear Model - Demand for money to be printed")
```

![](images/unnamed-chunk-20-1.png)

#### Split data for training and testing - Plot predictions vs test values

``` r
train_ratio <- 0.8

train <- subset(scaled_demand, year %in% sample(scaled_demand$year, train_ratio*nrow(scaled_demand)))
test <- subset(scaled_demand, !(year %in% train$year))

train.model <- lm(printed ~ GDP + interest, data=train)

predicted <- predict.lm(train.model,test[c("GDP", "interest")])
qplot(test$year*max(demand$year), predicted*max(demand$printed), color='predicted') +
  geom_point(aes(y = test$printed*max(demand$printed), color="actual")) +
  geom_text(aes(x = -Inf,y=Inf,hjust=0,vjust=1, label=sprintf("Mean error: %.2f%%", 100*mean(abs(test$printed-predicted))))) +
  labs(title="Predicted vs. Actual values - Money to be printed",
       y="Money",
       x="Year")
```

![](images/unnamed-chunk-21-1.png)

------------------------------------------------------------------------

Exercise 3: Generalized Linear Regression
-----------------------------------------

### Consider data published on n = 27 leukemia patients. The data (leukemiaremission.txt) has a response variable of whether leukemia remission occurred (REMISS), which is given by a 1.

``` r
leukemia <- read.csv("data/leukemiaremission.txt", sep="\t")
cols <- colnames(leukemia)
cols[1] <- "REMISS"
colnames(leukemia) <- cols
```

``` r
scaled_leuk <- data.frame(sapply(leukemia, FUN=scale_vector))
```

#### The predictor variables are cellularity of the marrow clot section (CELL), smear differential percentage of blasts (SMEAR), percentage of absolute marrow leukemia cell infiltrate (INFIL), percentage labeling index of the bone marrow leukemia cells (LI), absolute number of blasts in the peripheral blood (BLAST), and the highest temperature prior to start of treatment (TEMP).

#### Consider the response and explanatory variables and give the appropriate random and systematic component and the link function.

The response variable, `REMISS` is binary so the link function will be *logistic*. So, the random component will be Binomial, and the systematic component is likely mixed.

``` r
summary(leukemia[,c('CELL','SMEAR','INFIL','LI','BLAST','TEMP')])
```

    ##       CELL            SMEAR            INFIL              LI       
    ##  Min.   :0.2000   Min.   :0.3200   Min.   :0.0800   Min.   :0.400  
    ##  1st Qu.:0.8250   1st Qu.:0.4300   1st Qu.:0.3350   1st Qu.:0.650  
    ##  Median :0.9500   Median :0.6500   Median :0.6300   Median :0.900  
    ##  Mean   :0.8815   Mean   :0.6352   Mean   :0.5707   Mean   :1.004  
    ##  3rd Qu.:1.0000   3rd Qu.:0.8350   3rd Qu.:0.7400   3rd Qu.:1.250  
    ##  Max.   :1.0000   Max.   :0.9700   Max.   :0.9200   Max.   :1.900  
    ##      BLAST             TEMP       
    ##  Min.   :0.0000   Min.   :0.9800  
    ##  1st Qu.:0.2300   1st Qu.:0.9900  
    ##  Median :0.5200   Median :0.9900  
    ##  Mean   :0.6885   Mean   :0.9974  
    ##  3rd Qu.:1.0600   3rd Qu.:1.0050  
    ##  Max.   :2.0600   Max.   :1.0400

``` r
cor(scaled_leuk)
```

    ##            REMISS      CELL      SMEAR       INFIL         LI     BLAST
    ## REMISS  1.0000000 0.2645288  0.1994882  0.26399695  0.5419818 0.3611882
    ## CELL    0.2645288 1.0000000  0.2917881  0.60707674  0.1902352 0.4407724
    ## SMEAR   0.1994882 0.2917881  1.0000000  0.92970107  0.3174573 0.6123480
    ## INFIL   0.2639970 0.6070767  0.9297011  1.00000000  0.3211436 0.6957679
    ## LI      0.5419818 0.1902352  0.3174573  0.32114358  1.0000000 0.6038768
    ## BLAST   0.3611882 0.4407724  0.6123480  0.69576795  0.6038768 1.0000000
    ## TEMP   -0.1439632 0.1348323 -0.1203952 -0.03982526 -0.0540018 0.2166814
    ##               TEMP
    ## REMISS -0.14396315
    ## CELL    0.13483226
    ## SMEAR  -0.12039523
    ## INFIL  -0.03982526
    ## LI     -0.05400180
    ## BLAST   0.21668143
    ## TEMP    1.00000000

Based on the correlation matrix, `SMEAR` and `INFIL` are highly correlated. `INFIL` will be dropped because `SMEAR` is used later in the notebook.

#### Implement the model in R and interpret the model output.

``` r
leuk_model <- glm(REMISS ~ CELL + SMEAR + LI + BLAST + TEMP, data=scaled_leuk, binomial)
summary(leuk_model)
```

    ## 
    ## Call:
    ## glm(formula = REMISS ~ CELL + SMEAR + LI + BLAST + TEMP, family = binomial, 
    ##     data = scaled_leuk)
    ## 
    ## Deviance Residuals: 
    ##      Min        1Q    Median        3Q       Max  
    ## -1.85249  -0.69819  -0.04292   0.76793   1.62795  
    ## 
    ## Coefficients:
    ##             Estimate Std. Error z value Pr(>|z|)  
    ## (Intercept)   76.854     69.735   1.102   0.2704  
    ## CELL          11.639     10.345   1.125   0.2606  
    ## SMEAR          1.221      3.531   0.346   0.7294  
    ## LI             8.193      4.812   1.702   0.0887 .
    ## BLAST         -0.505      4.496  -0.112   0.9106  
    ## TEMP         -98.198     78.248  -1.255   0.2095  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## (Dispersion parameter for binomial family taken to be 1)
    ## 
    ##     Null deviance: 34.372  on 26  degrees of freedom
    ## Residual deviance: 21.748  on 21  degrees of freedom
    ## AIC: 33.748
    ## 
    ## Number of Fisher Scoring iterations: 7

``` r
exp(coef(leuk_model))
```

    ##  (Intercept)         CELL        SMEAR           LI        BLAST 
    ## 2.384188e+33 1.133801e+05 3.392089e+00 3.614825e+03 6.034817e-01 
    ##         TEMP 
    ## 2.254769e-43

``` r
exp(confint(leuk_model))
```

    ## Waiting for profiling to be done...

    ## Warning: glm.fit: fitted probabilities numerically 0 or 1 occurred

    ## Warning: glm.fit: fitted probabilities numerically 0 or 1 occurred

    ## Warning: glm.fit: fitted probabilities numerically 0 or 1 occurred

    ## Warning: glm.fit: fitted probabilities numerically 0 or 1 occurred

    ##                     2.5 %        97.5 %
    ## (Intercept)  2.428418e-18 7.099642e+103
    ## CELL         7.554552e-02  9.206794e+15
    ## SMEAR        2.799137e-03  6.457410e+03
    ## LI           2.847777e+00  6.402543e+08
    ## BLAST        5.270071e-05  4.647553e+03
    ## TEMP        4.405086e-125  2.579466e+11

#### Give the estimated odds of leukemia remission for LI = 0.9.

#### The odds ratio between the odds for two sets of predictors is given by

#### Compute the 95% Confidence intervals for the estimate for the predictor SMEAR.

``` r
cat(sprintf("\nThe confidence interval for SMEAR is [%s]", 
            toString(paste(confint(leuk_model)["SMEAR",]))))
```

    ## 
    ## The confidence interval for SMEAR is [-5.87844420003022, 8.77298353566014]
