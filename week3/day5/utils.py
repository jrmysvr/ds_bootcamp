"""
    Utility functions for twitter notebook
"""
from datetime import datetime
import time
import pandas as pd

def filepath(fname):
    return "data/{}".format(fname)

def get_text(tweet):
    output = ""
    if 'extended_tweet' in tweet:
        output = tweet['extended_tweet']['full_text']
    else:
        output = tweet['text']
        
    return output

def until_today():
    return datetime.today().strftime("%Y-%m-%d")

def collect_samples(twitter_streamer, n_samples=1, tweets_per_sample=100):
    """Use MyStreamer class to sample realtime tweets
    
    Arguments:
    n_samples -- number of samples to take from stream
    tweets_per_sample -- number of tweets collected per sample
    
    sample loop breaks when streamer status is ERROR
    """
    samples = list()
    for i in range(n_samples):
        tweet_samples = twitter_streamer.sample(max_count=tweets_per_sample)
        if twitter_streamer.status == twitter_streamer.ERROR:
            break
        samples += tweet_samples
        
    return list(filter(lambda t: 'delete' not in t, samples))

def sub_tweet(tweet):
    """Create simplified dictionary version of the twitter API tweet object"""
    sub = dict()
    sub['id'] = tweet['id_str']
    sub['date'] = tweet['created_at']
    sub['timestamp_ms'] = tweet['timestamp_ms'] \
                          if 'timestamp_ms' in tweet \
                          else date_to_timestamp(sub['date'])
    sub['hashtags'] = tweet['entities']['hashtags']
    sub['text'] = get_text(tweet)
    sub['screen_name'] = tweet['user']['screen_name']
    sub['user_name'] = tweet['user']['name']
    sub['user_id'] = tweet['user']['id_str']
    return sub

def gen_sub_tweets(tweet_list):
    return list(map(sub_tweet, tweet_list))

def get_tweets_until_today(twit, q):
    return twit.search(q=q, result_type="recent", count=10000, until=until_today())['statuses']    

def get_tweets(twit, q, result_type="popular"):
    return twit.search(q=q, result_type=result_type, count=10000)['statuses']

def get_hashtags(tweet):
    """Return hashtags from tweet object - compatible with sub_tweet()"""
    tag_text = []
    if 'hashtags' in tweet:
        for tag in tweet['hashtags']:
            tag_text.append(tag['text'])
    else:
        for tag in tweet['entities']['hashtags']:
            tag_text.append(get_text(tag))
    return tag_text

def get_user_info(tweet):
    """Create simplified dictionary version of the twitter API tweet object, user section"""
    user = tweet['user']
    tweet = sub_tweet(tweet)

    output = dict()
    output['tweet_id'] = tweet['id']
    output['tweet_date'] = tweet['date']
    output['id'] = user['id_str']
    output['followers'] = user['followers_count']
    output['name'] = user['name']
    output['screen_name'] = user['screen_name']
    output['friends'] = user['friends_count']
    
    return output

def list_hashtags(tweet_list):
    """Return all hashtags from list of tweets - compatible with sub_tweet()"""
    hashtags = []
    for tweet in tweet_list:
        tag_list = get_hashtags(tweet)
        if len(tag_list) > 0:
            hashtags += tag_list
            
    return hashtags

def gen_df_tweets(tweet_list, gen_sub=True):
    """Create pandas dataframe of list of twitter API tweet objects
    
    Optional argument:
    gen_sub -- return sub_tweet for each tweet object
    """
    if gen_sub:
        tweet_list = gen_sub_tweets(tweet_list)
    df =  pd.io.json.json_normalize(tweet_list)
    df = df.assign(num_hashtags=df.hashtags.apply(len))
    return df

def gen_df_tweet_search(twit, q):
    return gen_df_tweets(get_tweets(twit=twit, q=q))
  
def load_raw_tweets(fpath):
    with open(fpath, 'r', encoding='utf-8') as f:
        tweets = json.load(f)
        
    return gen_df_tweets(tweets)

def date_to_timestamp(date):
#     https://stackoverflow.com/questions/9637838/convert-string-date-to-timestamp-in-python
    return str(int(
        time.mktime(
            datetime.strptime(date, 
                              "%a %b %d %X +0000 %Y").timetuple())))

def big_round(num, powers):
    """ 
    Round numbers down by powers of 10
     - ex: big_round(123456, 3) ==> 123000
    """
    return round(num/pow(10, powers))*(pow(10, powers))

#def plot_hashtag_count_date(df):
#    """Create seaborn plot of number of hashtags per date - assume df has 'date' and 'num_hashtags' columns"""
#    g = sns.relplot(x='date', y='num_hashtags', data=df, aspect=3)
#    g.set_xticklabels(rotation=80)
#    return g

if __name__ == "__main__":
    print("Nothing to do here")
