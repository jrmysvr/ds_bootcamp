import os
import json
from twython import Twython, TwythonStreamer
from datetime import datetime
import atexit
import argparse
import sys

from private_keys import API_KEY, API_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET

class MyStreamer(TwythonStreamer):
    
    PATH = os.getcwd() + "/data"
    fname = lambda self, name:  "{}/{}.txt".format(self.PATH, name)
    fname_raw = lambda self, name: "{}/{}_raw.json".format(self.PATH, name)

    formatter = 10 * "#"
    banner = lambda self, text: "\n\n{} {} {}\n".format(self.formatter, text, self.formatter)
    
    # mode types
    FILTER = "FILTER"
    SAMPLE = "SAMPLE"

    # status types
    READY = "READY"
    ERROR = "ERROR"

    def __init__(self, *args):
        atexit.register(self._dump_fpath_raw)

        self.fpath = None
        self.fpath_raw = None
        
        self.tweet_list = list()
        self.tweet_count = 1
        self.error_count = 0
        self.retries = 0
        
        self.max_count = None
        self.max_retries = 3 

        self.mode = None
        self.status = self.READY

        super(MyStreamer, self).__init__(*args)

    def __del__(self):
        print("Terminating...")

    def _reset_counts(self):
        self.tweet_count = 1
        self.error_count = 0
        self.retries = 0
        self.max_count = None
        self.mode = None
        print("Reseting...")
    
    def _dump_fpath_raw(self):
        if self.mode == self.FILTER:
            if len(self.tweet_list) > 0:
                print("Saving tweet list to json")        
                with open(self.fpath_raw, 'w', encoding='utf-8') as f:
                    json.dump(self.tweet_list, f)

    def set_fpath(self, name):
        self.fpath = self.fname(name)
        self.fpath_raw = self.fname_raw(name)
        
    def set_max_count(self, count):
        self.max_count = count

    def set_mode(self, mode):
        self.mode = mode

    def _clean_name(self, name):
        return name.replace(',', '').replace(' ', '_')

    def filter(self, track, max_count=10):
        self.set_mode(self.FILTER)
        self.set_fpath(self._clean_name(track))
        
        with open(self.fpath, 'a+') as f:
            f.write(self.banner(datetime.now()))
            
        self.statuses.filter(track=track)
        return self.tweet_list 

    def sample(self, max_count=10):
        self.set_mode(self.SAMPLE)
        self.set_max_count(max_count)

        self.statuses.sample()
        return self.tweet_list

    def on_success(self, data):
        target_text = None
        if self.mode == self.FILTER: 
            if 'extended_tweet' in data:
                target_text = data['extended_tweet']['full_text']
            elif 'text' in data:
                target_text = data['text']
                
            if target_text is not None:
                try:
                    with open(self.fpath, 'a+') as f:
                        f.write(self.banner(self.tweet_count))
                        f.write(target_text)
                    
                    self.tweet_list.append(data)

                    print("Tweets found: {}".format(self.tweet_count), end="\r")
                    self.tweet_count += 1
                except Exception as e:
                    print(e)
                    self.error_count += 1
                    with open(self.fpath, 'r') as f:
                        lines = f.readlines()
                        lines = lines[:-5]
                        self.tweet_count -= 1
                    with open(self.fpath, 'w') as f:
                        for line in lines:
                            f.write(line)
                    
            if self.tweet_count > self.max_count:
                self.disconnect()
                print("Done - {} tweets collected, {} errors".format(self.tweet_count-1, self.error_count))
                self._reset_counts()

        if self.mode == self.SAMPLE:
            self.tweet_list.append(data)
            print("Tweet Samples found: {}".format(self.tweet_count), end="\r")
            self.tweet_count += 1

            if self.tweet_count > self.max_count:
                self.disconnect()
                print("Done - {} tweet samples collected".format(self.tweet_count-1))
                self._reset_counts()
             
    def on_error(self, status_code, data):
        print("Error:")
        print(status_code)
        print(self.max_retries - self.retries)
        if self.retries > self.max_retries:
            self.status = self.ERROR
            self.disconnect()

        self.retries += 1

if __name__ == "__main__":
    tokens = (API_KEY, API_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    stream = MyStreamer(*tokens)

    # TODO: Extend this functionality with separate class to handle arguments and another file to combine the streamer and argparser
    parser = argparse.ArgumentParser()
    parser.add_argument("--track", help="string of keywords to search")
    parser.add_argument("--max-count", help="max count of tweets to find")
    args, leftovers = parser.parse_known_args()
    
    if len(sys.argv) <= 1:
        search_track = "qanon, q-anon, q anon"
        stream.filter(track=search_track, max_count=1)
    else:
        max_count = 0
        max_count = int(args.max_count) if args.max_count else 0
        track = args.track if args.track else ""

        stream.filter(track=track, max_count=max_count)
        
