"""
    Translation tool utilizing googletrans module
"""

from googletrans import Translator, LANGUAGES

class GoogleTransTool:

    def __init__(self):
        self.trans_dict = dict()
        self.translator = Translator()

    def parse_translation(self, trans):
        """Return simplified translation object as returned by googletrans.Translator.translate() 
        using googletrans.LANGUAGES to identify language name
        """

        return (LANGUAGES[trans.src.lower()], trans.text)


    def translate_text(self, text):
        """Utilize googletrans.Translator to generate translation of text

        Use existing translation if it is found in self.trans_dict
        """

        if text in self.trans_dict:
            output = self.trans_dict[text]
        else:
            output = self.translator.translate(text)
            self.trans_dict[text] = output
        return output 