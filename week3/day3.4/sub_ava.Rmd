---
title: "Ava - Data Challenge"
output: html_notebook
# output: rmarkdown::github_document
# output: html_document
---

```{r opts, echo = FALSE}
knitr::opts_chunk$set(
  fig.path = "images/",
  dev = "png"
)
```

```{r, warning = F, message = F}
library(ggplot2)
library(car)
library(gclus)
library(egg)
library(sna)
library(corrplot)
```

Load Data
```{r}
ava <- read.csv("data/loggin_for_nitin.csv")
```

```{r, echo = F}
# head(ava)
```
### Column info

    BR - breathing rate
    Date - date of the recorded night (date in the morning of the ended recording)
    Deep sleep - percentage of deep sleep over the night
    HR - heart rate (pulse rate)
    HRV_SDNN - a heart rate variability parameter
    Light sleep - percentage of light sleep during the night
    Skin temperature - skin temperature 
    Time - equivalent of cycle day, starts at 0 and ends at the last day of the cycle, then it start over
    Total sleep - total sleeping time in seconds
    
```{r, echo = F}
# summary(ava)
```

### Cleaning
```{r}
# Normalize with z-score
normalize <- function(x) {
  return( (x - mean(x, na.rm = T)) / sd(x, na.rm = T) )
}

targets <- c("BR", "HR", "Deep_sleep", "Light_sleep", "HRV_SDNN", "Skin_temperature", "Total_sleep")
drop <- c("HRV_HF", "Awake", "Date")
ava.norm <- data.frame(sapply(ava[, targets], normalize))
ava.norm$ID <- ava$ID
ava.norm$Time <- ava$Time
others <- colnames(ava)[!(colnames(ava) %in% targets)]
```

---

```{r}
# aggregate column(s) vs. Time and omit all NA values
clean_group_time <- function(df, cols, fun = mean) {
  group.time <- aggregate(df[, cols], list(df$Time), fun)
  colnames(group.time)[1] <- "Time"
  return(na.omit(group.time))
}
```

### Explore correlation between physiological parameters, aggregated over Time (Cycle Day)
```{r}
sub.ava <- ava.norm[sample(nrow(ava), 100), ]
spm(clean_group_time(sub.ava, c("HR", "BR", "Skin_temperature")))
```

```{r}
# group all targeted variables over Time
group.time <- clean_group_time(sub.ava, targets)
print(targets)
```

```{r}
nrow(group.time)
```
##### Can the results generated using this dataframe be reliable with only 20 samples?

### Check relationship between parameters and Time
```{r, warning = F, message = F, fig.width = 12}
spm(group.time)
```

```{r}
cor(group.time)
```

```{r, fig.width = 12}
group.corr <- abs(cor(group.time))
group.colors <- dmat.color(group.corr)
group.o <- order.single(group.corr)
cpairs(group.time,
       group.o,
       panel.colors = group.colors,
       gap = 0.5,
       main = "Signals over Cycle duration - ordered and colored by correlation")
```
